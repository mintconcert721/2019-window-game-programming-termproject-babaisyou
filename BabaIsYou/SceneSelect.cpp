#include "stdafx.h"
#include "resource.h"
#include "SceneSelect.h"

SceneSelect::SceneSelect()
{
	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Map/StageSelectBackground.png", 854, 480, 3, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Map/map.ogg");

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_IntroSound = new Sound();
	m_IntroSound->CreateSound("Data/Sounds/intro.ogg");

	m_CanEnterScene = true;

	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			GridArray[garo][sero].SetPosition(garo * BLOCK_SIZE + (BLOCK_SIZE / 2), sero * BLOCK_SIZE + (BLOCK_SIZE / 2));
			GridArray[garo][sero].SetSize(BLOCK_SIZE);
			GridArray[garo][sero].SetKind(-1);
		}
	}

	Objects.resize(100);
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SetKind(-1);
		Objects[idx].SetVisible(false);
	}

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	Objects[0].SetPosition(GridArray[28][15].GetPosition().x, GridArray[28][15].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetImage(L"Data/Images/Map/text_baba.png", 24, 24, 3);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	Objects[1].SetPosition(GridArray[29][15].GetPosition().x, GridArray[29][15].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	Objects[2].SetPosition(GridArray[30][15].GetPosition().x, GridArray[30][15].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetImage(L"Data/Images/Map/text_you.png", 24, 24, 3);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	Objects[3].SetPosition(GridArray[28][16].GetPosition().x, GridArray[28][16].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetImage(L"Data/Images/Map/text_flag.png", 24, 24, 3);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	Objects[4].SetPosition(GridArray[29][16].GetPosition().x, GridArray[29][16].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	Objects[5].SetPosition(GridArray[30][16].GetPosition().x, GridArray[30][16].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetImage(L"Data/Images/Map/text_win.png", 24, 24, 3);

	Objects[6].SetKind(KIND_STAGE_TUTORIAL);
	Objects[6].SetPosition(GridArray[9][15].GetPosition().x, GridArray[9][15].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE / 2);
	Objects[6].SetVisible(true);
	Objects[6].SetImage(L"Data/Images/Map/Stage/n00.png", 16, 16, 1);

	Objects[7].SetKind(KIND_TARGET);
	Objects[7].SetPosition(GridArray[9][15].GetPosition().x, GridArray[9][15].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetPawn(true);
	Objects[7].SetProperty(PROPERTY::PUSH);
	Objects[7].SetImage(L"Data/Images/Map/Stage/target.png", 24, 24, 3);

	Objects[8].SetKind(KIND_STAGE_1);
	Objects[8].SetPosition(GridArray[10][13].GetPosition().x, GridArray[10][13].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE / 2);
	Objects[8].SetVisible(true);
	Objects[8].SetImage(L"Data/Images/Map/Stage/n01.png", 16, 16, 1);

	Objects[9].SetKind(KIND_STAGE_2);
	Objects[9].SetPosition(GridArray[10][12].GetPosition().x, GridArray[10][12].GetPosition().y);
	Objects[9].SetSize(BLOCK_SIZE / 2);
	Objects[9].SetVisible(true);
	Objects[9].SetImage(L"Data/Images/Map/Stage/n02.png", 16, 16, 1);

	Objects[10].SetKind(KIND_STAGE_3);
	Objects[10].SetPosition(GridArray[11][13].GetPosition().x, GridArray[11][13].GetPosition().y);
	Objects[10].SetSize(BLOCK_SIZE / 2);
	Objects[10].SetVisible(true);
	Objects[10].SetImage(L"Data/Images/Map/Stage/n03.png", 16, 16, 1);

	Objects[11].SetKind(KIND_STAGE_4);
	Objects[11].SetPosition(GridArray[11][12].GetPosition().x, GridArray[11][12].GetPosition().y);
	Objects[11].SetSize(BLOCK_SIZE / 2);
	Objects[11].SetVisible(true);
	Objects[11].SetImage(L"Data/Images/Map/Stage/n04.png", 16, 16, 1);

	m_LineImage1 = new ObjectImage();
	m_LineImage1->LoadSpriteImage(L"Data/Images/Map/Stage/right_bottom_line.png", 24, 24, 3, GridArray[10][15].GetPosition().x, GridArray[10][15].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_LineImage2 = new ObjectImage();
	m_LineImage2->LoadSpriteImage(L"Data/Images/Map/Stage/sero_line.png", 24, 24, 3, GridArray[10][14].GetPosition().x, GridArray[10][14].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);

	m_StageNameBackgroundImage1 = new ObjectImage();
	m_StageNameBackgroundImage1->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background.png", 24, 24, 3, GridArray[9][15].GetPosition().x, GridArray[9][15].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage2 = new ObjectImage();
	m_StageNameBackgroundImage2->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background.png", 24, 24, 3, GridArray[10][13].GetPosition().x, GridArray[10][13].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage3 = new ObjectImage();
	m_StageNameBackgroundImage3->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background.png", 24, 24, 3, GridArray[10][12].GetPosition().x, GridArray[10][12].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage4 = new ObjectImage();
	m_StageNameBackgroundImage4->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background.png", 24, 24, 3, GridArray[11][13].GetPosition().x, GridArray[11][13].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage14 = new ObjectImage();
	m_StageNameBackgroundImage14->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background.png", 24, 24, 3, GridArray[11][12].GetPosition().x, GridArray[11][12].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);

	m_StageNameBackgroundImage5 = new ObjectImage();
	m_StageNameBackgroundImage5->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_2.png", 24, 24, 3, GridArray[15][13].GetPosition().x, GridArray[15][13].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage6 = new ObjectImage();
	m_StageNameBackgroundImage6->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_2.png", 24, 24, 3, GridArray[15][10].GetPosition().x, GridArray[15][10].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	
	m_StageNameBackgroundImage7 = new ObjectImage();
	m_StageNameBackgroundImage7->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_1.png", 24, 24, 3, GridArray[12][8].GetPosition().x, GridArray[12][8].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage8 = new ObjectImage();
	m_StageNameBackgroundImage8->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_1.png", 24, 24, 3, GridArray[10][5].GetPosition().x, GridArray[10][5].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	m_StageNameBackgroundImage9 = new ObjectImage();
	m_StageNameBackgroundImage9->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_2.png", 24, 24, 3, GridArray[9][3].GetPosition().x, GridArray[9][3].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	
	m_StageNameBackgroundImage10 = new ObjectImage();
	m_StageNameBackgroundImage10->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_5.png", 24, 24, 3, GridArray[17][5].GetPosition().x, GridArray[17][5].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	
	m_StageNameBackgroundImage11 = new ObjectImage();
	m_StageNameBackgroundImage11->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_3.png", 24, 24, 3, GridArray[15][3].GetPosition().x, GridArray[15][3].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);

	m_StageNameBackgroundImage12 = new ObjectImage();
	m_StageNameBackgroundImage12->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_6.png", 24, 24, 3, GridArray[20][6].GetPosition().x, GridArray[20][6].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);
	
	m_StageNameBackgroundImage13 = new ObjectImage();
	m_StageNameBackgroundImage13->LoadSpriteImage(L"Data/Images/Map/Stage/stage_background_7.png", 24, 24, 3, GridArray[22][9].GetPosition().x, GridArray[22][9].GetPosition().y, BLOCK_SIZE, BLOCK_SIZE);

	m_DotImage1 = new ObjectImage();
	m_DotImage1->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[15][13].GetPosition().x, GridArray[15][13].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage2 = new ObjectImage();
	m_DotImage2->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[22][9].GetPosition().x, GridArray[22][9].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage3 = new ObjectImage();
	m_DotImage3->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[15][10].GetPosition().x, GridArray[15][10].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage4 = new ObjectImage();
	m_DotImage4->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[12][8].GetPosition().x, GridArray[12][8].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage5 = new ObjectImage();
	m_DotImage5->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[10][5].GetPosition().x, GridArray[10][5].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage6 = new ObjectImage();
	m_DotImage6->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[9][3].GetPosition().x, GridArray[9][3].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage7 = new ObjectImage();
	m_DotImage7->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[17][5].GetPosition().x, GridArray[17][5].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage8 = new ObjectImage();
	m_DotImage8->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[20][6].GetPosition().x, GridArray[20][6].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_DotImage9 = new ObjectImage();
	m_DotImage9->LoadSpriteImage(L"Data/Images/Map/Stage/dot.png", 24, 24, 3, GridArray[15][3].GetPosition().x, GridArray[15][3].GetPosition().y, BLOCK_SIZE / 3, BLOCK_SIZE / 3);

	m_Stage00NameImage = new ObjectImage();
	m_Stage00NameImage->LoadSpriteImage(L"Data/Images/Map/Stage/StageTutorialName.png", 241, 32, 1, GridArray[3][1].GetPosition().x, GridArray[3][1].GetPosition().y, BLOCK_SIZE * 4, BLOCK_SIZE / 2);

	m_Stage01NameImage = new ObjectImage();
	m_Stage01NameImage->LoadSpriteImage(L"Data/Images/Map/Stage/Stage01Name.png", 311, 34, 1, GridArray[3][1].GetPosition().x, GridArray[3][1].GetPosition().y, BLOCK_SIZE * 4, BLOCK_SIZE / 2);

	m_Stage02NameImage = new ObjectImage();
	m_Stage02NameImage->LoadSpriteImage(L"Data/Images/Map/Stage/Stage02Name.png", 378, 34, 1, GridArray[3][1].GetPosition().x + BLOCK_SIZE, GridArray[3][1].GetPosition().y, BLOCK_SIZE * 6, BLOCK_SIZE / 2);

	m_Stage03NameImage = new ObjectImage();
	m_Stage03NameImage->LoadSpriteImage(L"Data/Images/Map/Stage/Stage03Name.png", 266, 30, 1, GridArray[3][1].GetPosition().x, GridArray[3][1].GetPosition().y, BLOCK_SIZE * 4, BLOCK_SIZE / 2);

	m_Stage04NameImage = new ObjectImage();
	m_Stage04NameImage->LoadSpriteImage(L"Data/Images/Map/Stage/Stage04Name.png", 401, 30, 1, GridArray[3][1].GetPosition().x + BLOCK_SIZE, GridArray[3][1].GetPosition().y, BLOCK_SIZE * 6, BLOCK_SIZE / 2);

	m_SpaceImage = new ObjectImage();
	m_SpaceImage->LoadSpriteImage(L"Data/Images/Space.png", 103, 50, 1, GridArray[2][15].GetPosition().x, GridArray[2][15].GetPosition().y, BLOCK_SIZE * 2, BLOCK_SIZE);

	m_EnterTextImage = new ObjectImage();
	m_EnterTextImage->LoadSpriteImage(L"Data/Images/Map/Enter.png", 48, 24, 3, GridArray[2][16].GetPosition().x, GridArray[2][16].GetPosition().y, BLOCK_SIZE * 2, BLOCK_SIZE);

	m_KeyboardImage = new ObjectImage();
	m_KeyboardImage->LoadSpriteImage(L"Data/Images/Keyboard.png", 157, 104, 1, GridArray[5][15].GetPosition().x, GridArray[5][15].GetPosition().y - (BLOCK_SIZE / 2), BLOCK_SIZE * 3, BLOCK_SIZE * 2);

	m_MoveTextImage = new ObjectImage();
	m_MoveTextImage->LoadSpriteImage(L"Data/Images/Move.png", 48, 24, 3, GridArray[5][16].GetPosition().x, GridArray[5][16].GetPosition().y, BLOCK_SIZE * 2, BLOCK_SIZE);

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);
}

SceneSelect::~SceneSelect()
{
	delete m_BackgroundImage;

	delete m_LineImage1;
	delete m_LineImage2;

	delete m_StageNameBackgroundImage1;
	delete m_StageNameBackgroundImage2;
	delete m_StageNameBackgroundImage3;
	delete m_StageNameBackgroundImage4;

	delete m_StageNameBackgroundImage5;
	delete m_StageNameBackgroundImage6;

	delete m_StageNameBackgroundImage7;
	delete m_StageNameBackgroundImage8;
	delete m_StageNameBackgroundImage9;

	delete m_StageNameBackgroundImage10;

	delete m_StageNameBackgroundImage11;

	delete m_StageNameBackgroundImage12;

	delete m_StageNameBackgroundImage13;

	delete m_StageNameBackgroundImage14;

	delete m_DotImage1;
	delete m_DotImage2;
	delete m_DotImage3;
	delete m_DotImage4;
	delete m_DotImage5;
	delete m_DotImage6;
	delete m_DotImage7;
	delete m_DotImage8;
	delete m_DotImage9;

	delete m_Stage00NameImage;
	delete m_Stage01NameImage;
	delete m_Stage02NameImage;
	delete m_Stage03NameImage;
	delete m_Stage04NameImage;

	delete m_SpaceImage;
	delete m_EnterTextImage;
	delete m_KeyboardImage;
	delete m_MoveTextImage;

	delete m_FadeImage;

	delete m_Sound;
	delete m_MoveSound;
	delete m_IntroSound;
}

void SceneSelect::Init(HINSTANCE _hInstance)
{
	m_Sound->PlaySound(true, 1.0f);
	m_hInstance = _hInstance;
	
	FadeInit();
}

void SceneSelect::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage1->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage2->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage3->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage4->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage5->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage6->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage7->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage8->Render(_hdc, _hWnd);
	m_StageNameBackgroundImage9->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage10->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage11->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage12->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage13->Render(_hdc, _hWnd);

	m_StageNameBackgroundImage14->Render(_hdc, _hWnd);

	m_DotImage1->Render(_hdc, _hWnd);
	m_DotImage2->Render(_hdc, _hWnd);
	m_DotImage3->Render(_hdc, _hWnd);
	m_DotImage4->Render(_hdc, _hWnd);
	m_DotImage5->Render(_hdc, _hWnd);
	m_DotImage6->Render(_hdc, _hWnd);
	m_DotImage7->Render(_hdc, _hWnd);
	m_DotImage8->Render(_hdc, _hWnd);
	m_DotImage9->Render(_hdc, _hWnd);

	m_LineImage1->Render(_hdc, _hWnd);
	m_LineImage2->Render(_hdc, _hWnd);

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		if (Objects[idx].GetPawn() == true)
		{
			if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_TUTORIAL)
			{
				m_Stage00NameImage->Render(_hdc, _hWnd);
			}
			else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_1)
			{
				m_Stage01NameImage->Render(_hdc, _hWnd);
			}
			else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_2)
			{
				m_Stage02NameImage->Render(_hdc, _hWnd);
			}
			else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_3)
			{
				m_Stage03NameImage->Render(_hdc, _hWnd);
			}
			else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_4)
			{
				m_Stage04NameImage->Render(_hdc, _hWnd);
			}
		}
	}

	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			if (*m_CurrentGridMode)
			{
				hPen = CreatePen(PS_SOLID, 1, RGB(25, 75, 125));
				hOldPen = (HPEN)SelectObject(_hdc, hPen);
				hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				hOldBrush = (HBRUSH)SelectObject(_hdc, hBrush);

				Rectangle(_hdc, GridArray[garo][sero].GetPosition().x - (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y - (GridArray[garo][sero].GetSize().y / 2),
					GridArray[garo][sero].GetPosition().x + (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y + (GridArray[garo][sero].GetSize().y / 2));

				SelectObject(_hdc, hOldBrush);
				DeleteObject(hBrush);
				SelectObject(_hdc, hOldPen);
				DeleteObject(hPen);
			}
		}
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Render(_hdc, _hWnd);
	}

	m_SpaceImage->Render(_hdc, _hWnd);
	m_EnterTextImage->Render(_hdc, _hWnd);
	m_KeyboardImage->Render(_hdc, _hWnd);
	m_MoveTextImage->Render(_hdc, _hWnd);

	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneSelect::Update(HWND _hWnd)
{
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Update(_hWnd);
	}

	FadeUpdate();
}

void SceneSelect::SpriteUpdate(HWND _hWnd)
{
	m_BackgroundImage->Update(_hWnd);

	m_LineImage1->Update(_hWnd);
	m_LineImage2->Update(_hWnd);

	m_StageNameBackgroundImage1->Update(_hWnd);
	m_StageNameBackgroundImage2->Update(_hWnd);
	m_StageNameBackgroundImage3->Update(_hWnd);
	m_StageNameBackgroundImage4->Update(_hWnd);

	m_StageNameBackgroundImage5->Update(_hWnd);
	m_StageNameBackgroundImage6->Update(_hWnd);

	m_StageNameBackgroundImage7->Update(_hWnd);
	m_StageNameBackgroundImage8->Update(_hWnd);
	m_StageNameBackgroundImage9->Update(_hWnd);

	m_StageNameBackgroundImage10->Update(_hWnd);

	m_StageNameBackgroundImage11->Update(_hWnd);

	m_StageNameBackgroundImage12->Update(_hWnd);

	m_StageNameBackgroundImage13->Update(_hWnd);

	m_StageNameBackgroundImage14->Update(_hWnd);

	m_DotImage1->Update(_hWnd);
	m_DotImage2->Update(_hWnd);
	m_DotImage3->Update(_hWnd);
	m_DotImage4->Update(_hWnd);
	m_DotImage5->Update(_hWnd);
	m_DotImage6->Update(_hWnd);
	m_DotImage7->Update(_hWnd);
	m_DotImage8->Update(_hWnd);
	m_DotImage9->Update(_hWnd);

	m_Stage00NameImage->Update(_hWnd);
	m_Stage01NameImage->Update(_hWnd);
	m_Stage02NameImage->Update(_hWnd);
	m_Stage03NameImage->Update(_hWnd);
	m_Stage04NameImage->Update(_hWnd);

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SpriteUpdate(_hWnd);
	}

	m_SpaceImage->Update(_hWnd);
	m_EnterTextImage->Update(_hWnd);
	m_KeyboardImage->Update(_hWnd);
	m_MoveTextImage->Update(_hWnd);
}

void SceneSelect::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			m_FadeOut = true;
			m_NextGameState = SETTING;
		}
		break;
		case VK_UP:
		case VK_DOWN:
		case VK_LEFT:
		case VK_RIGHT:
		{
			for (auto idx = 0; idx < Objects.size(); idx++)
			{
				if (Objects[idx].GetPawn() == true)
				{
					Objects[idx].KeyboardMessage(hWnd, message, wParam, lParam);
					m_MoveSound->PlaySound(false, 1.0f);
				}
			}
		}
		break;
		case VK_SPACE:
		{
			for (auto idx = 0; idx < Objects.size(); idx++)
			{
				if (Objects[idx].GetPawn() == true)
				{
					if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_TUTORIAL)
					{
						if (m_StageState[TUTORIAL] == true)
						{
							m_FadeOut = true;
							m_NextGameState = TUTORIAL;
							m_IntroSound->PlaySound(false, 1.0f);
						}
						else
							DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
					}
					else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_1)
					{
						if (m_StageState[STAGE1] == true)
						{
							m_FadeOut = true;
							m_NextGameState = STAGE1;
							m_IntroSound->PlaySound(false, 1.0f);
						}
						else
							DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
					}
					else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_2)
					{
						if (m_StageState[STAGE2] == true)
						{
							m_FadeOut = true;
							m_NextGameState = STAGE2;
							m_IntroSound->PlaySound(false, 1.0f);
						}
						else
							DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
					}
					else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_3)
					{
						if (m_StageState[STAGE3] == true)
						{
							m_FadeOut = true;
							m_NextGameState = STAGE3;
							m_IntroSound->PlaySound(false, 1.0f);
						}
						else
							DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
					}
					else if (Objects[idx].CollisionCheck(Objects) == KIND_STAGE_4)
					{
						if (m_StageState[STAGE4] == true)
						{
							m_FadeOut = true;
							m_NextGameState = STAGE4;
							m_IntroSound->PlaySound(false, 1.0f);
						}
						else
							DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
					}
				}
			}
		}
		break;
		}
	}
}

void SceneSelect::StopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;

	m_Sound->StopSound();
}

void SceneSelect::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneSelect::GetStageState(bool * state, int size)
{
	for (int idx = 0; idx < size; idx++)
	{
		m_StageState[idx] = state[idx];
	}
}

void SceneSelect::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

const bool SceneSelect::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneSelect::GetGridMode(bool * value)
{
	m_CurrentGridMode = value;
}

void SceneSelect::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneSelect::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneSelect::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{

	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneSelect::FadeIn()
{
	m_alpha -= ALPHA_SPEED;
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneSelect::FadeOut()
{
	m_alpha += ALPHA_SPEED;
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}

BOOL CALLBACK SceneSelect::DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hDialogDC, hDialogMemDC, hDialogBitDC;
	static RECT DialogCRT; // 직사각형모양 범 위 선언
	PAINTSTRUCT DialogPS;
	static HBITMAP hDialogBitmap, hDialogOldBitmap;

	static HBITMAP hButtonBitmap;

	static HWND _hWnd;

	static ObjectImage* DialogBackground;
	static 	ObjectImage* DialogNotifyImage;
	static 	ObjectImage* DialogSpaceImage;
	static 	ObjectImage* DialogEnterImage;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		_hWnd = (HWND)GetWindowLongPtrA(hWnd, GWLP_HWNDPARENT);
		GetClientRect(hWnd, &DialogCRT);

		DialogBackground = new ObjectImage();
		DialogBackground->LoadSpriteImage(L"Data/Images/Title/TitleBackground.png", 792, 432, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2), DialogCRT.right, DialogCRT.bottom);

		DialogNotifyImage = new ObjectImage();
		DialogNotifyImage->LoadSpriteImage(L"Data/Images/Notify.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogSpaceImage = new ObjectImage();
		DialogSpaceImage->LoadSpriteImage(L"Data/Images/Space.png", 103, 50, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 25, 70, 50);

		DialogEnterImage = new ObjectImage();
		DialogEnterImage->LoadSpriteImage(L"Data/Images/Map/Enter.png", 48, 24, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 75, 70, 50);

		SetTimer(hWnd, 3, 1, NULL);
		SetTimer(hWnd, 4, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 3:
		{
			if (GetAsyncKeyState(VK_RETURN) & 0x8000)
			{
				delete DialogBackground;
				delete DialogNotifyImage;
				delete DialogSpaceImage;
				delete DialogEnterImage;

				EndDialog(hWnd, 0);
			}
		}
		break;
		case 4:
		{
			if (DialogBackground)
				DialogBackground->Update(hWnd);

			if(DialogNotifyImage)
				DialogNotifyImage->Update(hWnd);

			if (DialogSpaceImage)
				DialogSpaceImage->Update(hWnd);

			if (DialogEnterImage)
				DialogEnterImage->Update(hWnd);
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hDialogDC = BeginPaint(hWnd, &DialogPS);

		hDialogMemDC = CreateCompatibleDC(hDialogDC);
		hDialogBitmap = CreateCompatibleBitmap(hDialogDC, DialogCRT.right, DialogCRT.bottom);
		hDialogOldBitmap = (HBITMAP)SelectObject(hDialogMemDC, hDialogBitmap);
		FillRect(hDialogMemDC, &DialogCRT, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		if(DialogBackground)
			DialogBackground->Render(hDialogMemDC, hWnd);

		if (DialogNotifyImage)
			DialogNotifyImage->Render(hDialogMemDC, hWnd);

		if (DialogSpaceImage)
			DialogSpaceImage->Render(hDialogMemDC, hWnd);

		if (DialogEnterImage)
			DialogEnterImage->Render(hDialogMemDC, hWnd);
	
		BitBlt(hDialogDC, 0, 0, DialogCRT.right, DialogCRT.bottom, hDialogMemDC, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(hDialogMemDC, hDialogOldBitmap));
		DeleteDC(hDialogMemDC);

		EndPaint(hWnd, &DialogPS);
	}
	break;
	}
	return 0;
}