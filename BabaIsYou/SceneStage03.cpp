#include "stdafx.h"
#include "resource.h"
#include "SceneStage03.h"
#include "Baba.h"

SceneStage03::SceneStage03()
{
	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			GridArray[garo][sero].SetPosition(garo * BLOCK_SIZE + (BLOCK_SIZE / 2), sero * BLOCK_SIZE + (BLOCK_SIZE / 2));
			GridArray[garo][sero].SetSize(BLOCK_SIZE);
			GridArray[garo][sero].SetKind(-1);
		}
	}

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Map/BG4.wav");

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_CompleteSound = new Sound();
	m_CompleteSound->CreateSound("Data/Sounds/complete.ogg");

	m_DefeatSound = new Sound();
	m_DefeatSound->CreateSound("Data/Sounds/defeat.ogg");

	m_Player = new Baba();
	m_Player->SetKind(OBJECT_KIND::WALL);
	int px = 14, py = 5;
	m_Player->SetPosition(GridArray[px][py].GetPosition().x, GridArray[px][py].GetPosition().y);

	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Tutorial/TutorialBackground.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	int a, b;

	tiles.resize(30);
	for (auto idx = 0; idx < tiles.size(); idx++)
	{
		tiles[idx] = new ObjectImage();
		tiles[idx]->LoadSpriteImage(L"Data/Images/Tutorial/Tutorial_tile.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	}

	a= 13, b = 4;
	for (auto idx = 0; idx < 6; idx++)
	{
		tiles[idx]->SetPosition(GridArray[a + idx][b].GetPosition().x, GridArray[a + idx][b].GetPosition().y);
	}
	b = 5;
	for (auto idx = 6; idx < 12; idx++)
	{
		tiles[idx]->SetPosition(GridArray[a + idx - 6][b].GetPosition().x, GridArray[a + idx - 6][b].GetPosition().y);
	}
	b = 6;
	for (auto idx = 12; idx < 18; idx++)
	{
		tiles[idx]->SetPosition(GridArray[a + idx - 12][b].GetPosition().x, GridArray[a + idx - 12][b].GetPosition().y);
	}
	b = 7;
	for (auto idx = 18; idx < 24; idx++)
	{
		tiles[idx]->SetPosition(GridArray[a + idx - 18][b].GetPosition().x, GridArray[a + idx - 18][b].GetPosition().y);
	}
	b = 8;
	for (auto idx = 24; idx < 30; idx++)
	{
		tiles[idx]->SetPosition(GridArray[a + idx - 24][b].GetPosition().x, GridArray[a + idx - 24][b].GetPosition().y);
	}

	grass_tiles.resize(18);
	for (auto idx = 0; idx < grass_tiles.size(); idx++)
	{
		grass_tiles[idx] = new ObjectImage();
	}

	grass_tiles[0]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 21, b = 2;
	grass_tiles[0]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[1]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 21, b = 3;
	grass_tiles[1]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[2]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 24, b = 9;
	grass_tiles[2]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[3]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 25, b = 11;
	grass_tiles[3]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[4]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 23, b = 14;
	grass_tiles[4]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[5]->LoadSpriteImage(L"Data/Images/Stage02/grass_9.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 24, b = 15;
	grass_tiles[5]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[6]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 25, b = 15;
	grass_tiles[6]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[7]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 24, b = 16;
	grass_tiles[7]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[8]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 6, b = 14;
	grass_tiles[8]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[9]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 6, b = 15;
	grass_tiles[9]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[10]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 7, b = 14;
	grass_tiles[10]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[11]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 7, b = 15;
	grass_tiles[11]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[12]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 5, b = 9;
	grass_tiles[12]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[13]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 6, b = 10;
	grass_tiles[13]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[14]->LoadSpriteImage(L"Data/Images/Stage02/grass_2.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 8, b = 7;
	grass_tiles[14]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[15]->LoadSpriteImage(L"Data/Images/Stage02/grass_4.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 9, b = 7;
	grass_tiles[15]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[16]->LoadSpriteImage(L"Data/Images/Stage02/grass.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 9, b = 3;
	grass_tiles[16]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[17]->LoadSpriteImage(L"Data/Images/Stage02/grass.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 10, b = 5;
	grass_tiles[17]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);


	Objects.resize(130);
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SetKind(-1);
		Objects[idx].SetVisible(false);
	}

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 5, b = 2;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetImage(L"Data/Images/Map/text_baba.png", 24, 24, 3);
	Objects[0].SetProperty(PROPERTY::PUSH);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 5, b = 3;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[1].SetProperty(PROPERTY::PUSH);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 5, b = 4;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetImage(L"Data/Images/Map/text_you.png", 24, 24, 3);
	Objects[2].SetProperty(PROPERTY::PUSH);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 18, b = 14;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetImage(L"Data/Images/Map/text_flag.png", 24, 24, 3);
	Objects[3].SetProperty(PROPERTY::PUSH);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 19, b = 14;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[4].SetProperty(PROPERTY::PUSH);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 20, b = 14;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetImage(L"Data/Images/Map/text_win.png", 24, 24, 3);
	Objects[5].SetProperty(PROPERTY::PUSH);

	Objects[6].SetKind(TEXT_KIND::TEXT_WALL);
	a = 6, b = 2;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetImage(L"Data/Images/Tutorial/text_wall.png", 24, 24, 3);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 6, b = 3;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_STOP);
	a = 6, b = 4;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetImage(L"Data/Images/Tutorial/text_stop.png", 24, 24, 3);
	Objects[8].SetProperty(PROPERTY::PUSH);

	Objects[9].SetKind(TEXT_KIND::TEXT_ROCK);
	a = 18, b = 11;
	Objects[9].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[9].SetSize(BLOCK_SIZE);
	Objects[9].SetVisible(true);
	Objects[9].SetImage(L"Data/Images/Tutorial/text_rock.png", 24, 24, 3);
	Objects[9].SetProperty(PROPERTY::PUSH);

	Objects[10].SetKind(TEXT_KIND::TEXT_IS);
	a = 19, b = 11;
	Objects[10].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[10].SetSize(BLOCK_SIZE);
	Objects[10].SetVisible(true);
	Objects[10].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[10].SetProperty(PROPERTY::PUSH);

	Objects[11].SetKind(TEXT_KIND::TEXT_PUSH);
	a = 20, b = 11;
	Objects[11].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[11].SetSize(BLOCK_SIZE);
	Objects[11].SetVisible(true);
	Objects[11].SetImage(L"Data/Images/Tutorial/text_push.png", 24, 24, 3);
	Objects[11].SetProperty(PROPERTY::PUSH);

	Objects[12].SetKind(TEXT_KIND::TEXT_WATER);
	a = 11, b = 6;
	Objects[12].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[12].SetSize(BLOCK_SIZE);
	Objects[12].SetVisible(true);
	Objects[12].SetImage(L"Data/Images/Stage03/text_water.png", 24, 24, 3);
	Objects[12].SetProperty(PROPERTY::PUSH);

	Objects[13].SetKind(TEXT_KIND::TEXT_IS);
	a = 11, b = 7;
	Objects[13].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[13].SetSize(BLOCK_SIZE);
	Objects[13].SetVisible(true);
	Objects[13].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[13].SetProperty(PROPERTY::PUSH);

	Objects[14].SetKind(TEXT_KIND::TEXT_SINK);
	a = 11, b = 8;
	Objects[14].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[14].SetSize(BLOCK_SIZE);
	Objects[14].SetVisible(true);
	Objects[14].SetImage(L"Data/Images/Stage03/text_sink.png", 24, 24, 3);
	Objects[14].SetProperty(PROPERTY::PUSH);

	Objects[15].SetKind(OBJECT_KIND::WALL);
	a = 12, b = 3;
	Objects[15].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[15].SetSize(BLOCK_SIZE);
	Objects[15].SetVisible(true);
	Objects[15].SetImage(L"Data/Images/Stage01/wall_10.png", 24, 24, 3);
	Objects[15].SetProperty(PROPERTY::STOP);

	Objects[16].SetKind(OBJECT_KIND::WALL);
	a = 19, b = 3;
	Objects[16].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[16].SetSize(BLOCK_SIZE);
	Objects[16].SetVisible(true);
	Objects[16].SetImage(L"Data/Images/Stage01/wall_13.png", 24, 24, 3);
	Objects[16].SetProperty(PROPERTY::STOP);

	Objects[17].SetKind(OBJECT_KIND::WALL);
	a = 9, b = 9;
	Objects[17].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[17].SetSize(BLOCK_SIZE);
	Objects[17].SetVisible(true);
	Objects[17].SetImage(L"Data/Images/Stage01/wall_10.png", 24, 24, 3);
	Objects[17].SetProperty(PROPERTY::STOP);

	Objects[18].SetKind(OBJECT_KIND::WALL);
	a = 12, b = 9;
	Objects[18].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[18].SetSize(BLOCK_SIZE);
	Objects[18].SetVisible(true);
	Objects[18].SetImage(L"Data/Images/Stage01/wall_7.png", 24, 24, 3);
	Objects[18].SetProperty(PROPERTY::STOP);

	Objects[19].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 9;
	Objects[19].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[19].SetSize(BLOCK_SIZE);
	Objects[19].SetVisible(true);
	Objects[19].SetImage(L"Data/Images/Stage01/wall_10.png", 24, 24, 3);
	Objects[19].SetProperty(PROPERTY::STOP);

	Objects[20].SetKind(OBJECT_KIND::WALL);
	a = 19, b = 9;
	Objects[20].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[20].SetSize(BLOCK_SIZE);
	Objects[20].SetVisible(true);
	Objects[20].SetImage(L"Data/Images/Stage01/wall_8.png", 24, 24, 3);
	Objects[20].SetProperty(PROPERTY::STOP);

	Objects[21].SetKind(OBJECT_KIND::WALL);
	a = 22, b = 9;
	Objects[21].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[21].SetSize(BLOCK_SIZE);
	Objects[21].SetVisible(true);
	Objects[21].SetImage(L"Data/Images/Stage01/wall_13.png", 24, 24, 3);
	Objects[21].SetProperty(PROPERTY::STOP);

	Objects[22].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 12;
	Objects[22].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[22].SetSize(BLOCK_SIZE);
	Objects[22].SetVisible(true);
	Objects[22].SetImage(L"Data/Images/Stage01/wall_2.png", 24, 24, 3);
	Objects[22].SetProperty(PROPERTY::STOP);

	Objects[23].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 14;
	Objects[23].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[23].SetSize(BLOCK_SIZE);
	Objects[23].SetVisible(true);
	Objects[23].SetImage(L"Data/Images/Stage01/wall_9.png", 24, 24, 3);
	Objects[23].SetProperty(PROPERTY::STOP);

	Objects[24].SetKind(OBJECT_KIND::WALL);
	a = 14, b = 13;
	Objects[24].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[24].SetSize(BLOCK_SIZE);
	Objects[24].SetVisible(true);
	Objects[24].SetImage(L"Data/Images/wall_1.png", 24, 24, 3);
	Objects[24].SetProperty(PROPERTY::STOP);

	Objects[25].SetKind(OBJECT_KIND::WALL);
	a = 9, b = 16;
	Objects[25].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[25].SetSize(BLOCK_SIZE);
	Objects[25].SetVisible(true);
	Objects[25].SetImage(L"Data/Images/Stage01/wall_4.png", 24, 24, 3);
	Objects[25].SetProperty(PROPERTY::STOP);

	Objects[26].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 16;
	Objects[26].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[26].SetSize(BLOCK_SIZE);
	Objects[26].SetVisible(true);
	Objects[26].SetImage(L"Data/Images/Stage01/wall_8.png", 24, 24, 3);
	Objects[26].SetProperty(PROPERTY::STOP);

	Objects[27].SetKind(OBJECT_KIND::WALL);
	a = 22, b = 16;
	Objects[27].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[27].SetSize(BLOCK_SIZE);
	Objects[27].SetVisible(true);
	Objects[27].SetImage(L"Data/Images/Stage01/wall_7.png", 24, 24, 3);
	Objects[27].SetProperty(PROPERTY::STOP);

	for (auto idx = 28; idx <34; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 15][3].GetPosition().x, GridArray[idx - 15][3].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 34; idx < 36; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 24][9].GetPosition().x, GridArray[idx - 24][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 36; idx < 38; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 19][9].GetPosition().x, GridArray[idx - 19][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 38; idx < 40; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 18][9].GetPosition().x, GridArray[idx - 18][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 40; idx < 46; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 30][16].GetPosition().x, GridArray[idx - 30][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 46; idx < 51; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 29][16].GetPosition().x, GridArray[idx - 29][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 51; idx < 56; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[12][idx - 47].GetPosition().x, GridArray[12][idx - 47].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 56; idx < 61; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[19][idx - 52].GetPosition().x, GridArray[19][idx - 52].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 61; idx < 67; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[9][idx - 51].GetPosition().x, GridArray[9][idx - 51].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 67; idx < 73; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[22][idx - 57].GetPosition().x, GridArray[22][idx - 57].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[73].SetKind(OBJECT_KIND::WALL);
	Objects[73].SetPosition(GridArray[16][15].GetPosition().x, GridArray[16][15].GetPosition().y);
	Objects[73].SetSize(BLOCK_SIZE);
	Objects[73].SetVisible(true);
	Objects[73].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
	Objects[73].SetProperty(PROPERTY::STOP);

	for (auto idx = 74; idx < 76; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[16][idx - 64].GetPosition().x, GridArray[16][idx - 64].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[76].SetKind(OBJECT_KIND::ROCK);
	Objects[76].SetPosition(GridArray[17][7].GetPosition().x, GridArray[17][7].GetPosition().y);
	Objects[76].SetSize(BLOCK_SIZE);
	Objects[76].SetVisible(true);
	Objects[76].SetImage(L"Data/Images/Tutorial/rock.png", 24, 24, 3);
	Objects[76].SetProperty(PROPERTY::PUSH);

	Objects[77].SetKind(OBJECT_KIND::ROCK);
	Objects[77].SetPosition(GridArray[20][12].GetPosition().x, GridArray[20][12].GetPosition().y);
	Objects[77].SetSize(BLOCK_SIZE);
	Objects[77].SetVisible(true);
	Objects[77].SetImage(L"Data/Images/Tutorial/rock.png", 24, 24, 3);
	Objects[77].SetProperty(PROPERTY::PUSH);

	for (auto idx = 78; idx < 83; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[7][idx - 78].GetPosition().x, GridArray[7][idx - 78].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage01/wall_11.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[83].SetKind(OBJECT_KIND::WALL);
	Objects[83].SetPosition(GridArray[7][5].GetPosition().x, GridArray[7][5].GetPosition().y);
	Objects[83].SetSize(BLOCK_SIZE);
	Objects[83].SetVisible(true);
	Objects[83].SetImage(L"Data/Images/Stage01/wall_7.png", 24, 24, 3);
	Objects[83].SetProperty(PROPERTY::STOP);

	for (auto idx = 84; idx < 91; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 84][5].GetPosition().x, GridArray[idx - 84][5].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/wall_6.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 91; idx < 98; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 91][0].GetPosition().x, GridArray[idx - 91][0].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage03/flower.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 98; idx < 105; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 98][1].GetPosition().x, GridArray[idx - 98][1].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage03/flower.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 105; idx < 110; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 105][2].GetPosition().x, GridArray[idx - 105][2].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage03/flower.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 110; idx < 115; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 110][3].GetPosition().x, GridArray[idx - 110][3].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage03/flower.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 115; idx < 120; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 115][4].GetPosition().x, GridArray[idx - 115][4].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage03/flower.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[120].SetKind(OBJECT_KIND::WATER);
	Objects[120].SetPosition(GridArray[15][10].GetPosition().x, GridArray[15][10].GetPosition().y);
	Objects[120].SetSize(BLOCK_SIZE);
	Objects[120].SetVisible(true);
	Objects[120].SetImage(L"Data/Images/Stage03/water_11.png", 24, 24, 3);
	Objects[120].SetProperty(PROPERTY::OVERRIDE);

	Objects[121].SetKind(OBJECT_KIND::WATER);
	Objects[121].SetPosition(GridArray[10][13].GetPosition().x, GridArray[10][13].GetPosition().y);
	Objects[121].SetSize(BLOCK_SIZE);
	Objects[121].SetVisible(true);
	Objects[121].SetImage(L"Data/Images/Stage03/water_9.png", 24, 24, 3);
	Objects[121].SetProperty(PROPERTY::OVERRIDE);

	Objects[122].SetKind(OBJECT_KIND::WATER);
	Objects[122].SetPosition(GridArray[12][13].GetPosition().x, GridArray[12][13].GetPosition().y);
	Objects[122].SetSize(BLOCK_SIZE);
	Objects[122].SetVisible(true);
	Objects[122].SetImage(L"Data/Images/Stage03/water_12.png", 24, 24, 3);
	Objects[122].SetProperty(PROPERTY::OVERRIDE);

	Objects[123].SetKind(OBJECT_KIND::FLAG); // 플래그
	Objects[123].SetPosition(GridArray[10][15].GetPosition().x, GridArray[10][15].GetPosition().y);
	Objects[123].SetSize(BLOCK_SIZE);
	Objects[123].SetVisible(true);
	Objects[123].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
	Objects[123].SetProperty(PROPERTY::OVERRIDE);

	Objects[124].SetKind(OBJECT_KIND::WATER);
	Objects[124].SetPosition(GridArray[12][15].GetPosition().x, GridArray[12][15].GetPosition().y);
	Objects[124].SetSize(BLOCK_SIZE);
	Objects[124].SetVisible(true);
	Objects[124].SetImage(L"Data/Images/Stage03/water_6.png", 24, 24, 3);
	Objects[124].SetProperty(PROPERTY::OVERRIDE);

	Objects[125].SetKind(OBJECT_KIND::WATER);
	Objects[125].SetPosition(GridArray[11][13].GetPosition().x, GridArray[11][13].GetPosition().y);
	Objects[125].SetSize(BLOCK_SIZE);
	Objects[125].SetVisible(true);
	Objects[125].SetImage(L"Data/Images/Stage03/water_13.png", 24, 24, 3);
	Objects[125].SetProperty(PROPERTY::OVERRIDE);

	Objects[126].SetKind(OBJECT_KIND::WATER);
	Objects[126].SetPosition(GridArray[11][14].GetPosition().x, GridArray[11][14].GetPosition().y);
	Objects[126].SetSize(BLOCK_SIZE);
	Objects[126].SetVisible(true);
	Objects[126].SetImage(L"Data/Images/Stage03/water_15.png", 24, 24, 3);
	Objects[126].SetProperty(PROPERTY::OVERRIDE);

	Objects[127].SetKind(OBJECT_KIND::WATER);
	Objects[127].SetPosition(GridArray[11][15].GetPosition().x, GridArray[11][15].GetPosition().y);
	Objects[127].SetSize(BLOCK_SIZE);
	Objects[127].SetVisible(true);
	Objects[127].SetImage(L"Data/Images/Stage03/water_3.png", 24, 24, 3);
	Objects[127].SetProperty(PROPERTY::OVERRIDE);

	Objects[128].SetKind(OBJECT_KIND::WATER);
	Objects[128].SetPosition(GridArray[10][14].GetPosition().x, GridArray[10][14].GetPosition().y);
	Objects[128].SetSize(BLOCK_SIZE);
	Objects[128].SetVisible(true);
	Objects[128].SetImage(L"Data/Images/Stage03/water_3.png", 24, 24, 3);
	Objects[128].SetProperty(PROPERTY::OVERRIDE);

	Objects[129].SetKind(OBJECT_KIND::WATER);
	Objects[129].SetPosition(GridArray[12][14].GetPosition().x, GridArray[12][14].GetPosition().y);
	Objects[129].SetSize(BLOCK_SIZE);
	Objects[129].SetVisible(true);
	Objects[129].SetImage(L"Data/Images/Stage03/water_14.png", 24, 24, 3);
	Objects[129].SetProperty(PROPERTY::OVERRIDE);
}

SceneStage03::~SceneStage03()
{
	delete m_Player;

	delete m_BackgroundImage;
	delete m_FadeImage;

	for (auto& v : tiles)
	{
		if (v)
			delete v;
	}

	for (auto& v : grass_tiles)
	{
		if (v)
			delete v;
	}

	delete m_Sound;
	delete m_MoveSound;
	delete m_CompleteSound;
	delete m_DefeatSound;
}

void SceneStage03::Init(HINSTANCE _hInstance)
{
	m_hInstance = _hInstance;

	m_Sound->PlaySound(true, 0.1f);

	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;

	m_Timer = 0;

	if (m_Player)
	{
		m_Player->SetLife(true);
		m_Player->SetCanMove(true);
		m_Player->SetPosition(GridArray[14][5].GetPosition().x, GridArray[14][5].GetPosition().y);
		m_Player->SetKind(OBJECT_KIND::BABA);
	}

	int a, b;

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 5, b = 2;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetProperty(PROPERTY::PUSH);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 5, b = 3;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetProperty(PROPERTY::PUSH);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 5, b = 4;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetProperty(PROPERTY::PUSH);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 18, b = 14;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetProperty(PROPERTY::PUSH);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 19, b = 14;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetProperty(PROPERTY::PUSH);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 20, b = 14;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetProperty(PROPERTY::PUSH);

	Objects[6].SetKind(TEXT_KIND::TEXT_WALL);
	a = 6, b = 2;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 6, b = 3;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_STOP);
	a = 6, b = 4;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetProperty(PROPERTY::PUSH);

	Objects[9].SetKind(TEXT_KIND::TEXT_ROCK);
	a = 18, b = 11;
	Objects[9].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[9].SetSize(BLOCK_SIZE);
	Objects[9].SetVisible(true);
	Objects[9].SetProperty(PROPERTY::PUSH);

	Objects[10].SetKind(TEXT_KIND::TEXT_IS);
	a = 19, b = 11;
	Objects[10].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[10].SetSize(BLOCK_SIZE);
	Objects[10].SetVisible(true);
	Objects[10].SetProperty(PROPERTY::PUSH);

	Objects[11].SetKind(TEXT_KIND::TEXT_PUSH);
	a = 20, b = 11;
	Objects[11].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[11].SetSize(BLOCK_SIZE);
	Objects[11].SetVisible(true);
	Objects[11].SetProperty(PROPERTY::PUSH);

	Objects[12].SetKind(TEXT_KIND::TEXT_WATER);
	a = 11, b = 6;
	Objects[12].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[12].SetSize(BLOCK_SIZE);
	Objects[12].SetVisible(true);
	Objects[12].SetProperty(PROPERTY::PUSH);

	Objects[13].SetKind(TEXT_KIND::TEXT_IS);
	a = 11, b = 7;
	Objects[13].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[13].SetSize(BLOCK_SIZE);
	Objects[13].SetVisible(true);
	Objects[13].SetProperty(PROPERTY::PUSH);

	Objects[14].SetKind(TEXT_KIND::TEXT_SINK);
	a = 11, b = 8;
	Objects[14].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[14].SetSize(BLOCK_SIZE);
	Objects[14].SetVisible(true);
	Objects[14].SetProperty(PROPERTY::PUSH);

	Objects[15].SetKind(OBJECT_KIND::WALL);
	a = 12, b = 3;
	Objects[15].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[15].SetSize(BLOCK_SIZE);
	Objects[15].SetVisible(true);
	Objects[15].SetProperty(PROPERTY::STOP);

	Objects[16].SetKind(OBJECT_KIND::WALL);
	a = 19, b = 3;
	Objects[16].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[16].SetSize(BLOCK_SIZE);
	Objects[16].SetVisible(true);
	Objects[16].SetProperty(PROPERTY::STOP);

	Objects[17].SetKind(OBJECT_KIND::WALL);
	a = 9, b = 9;
	Objects[17].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[17].SetSize(BLOCK_SIZE);
	Objects[17].SetVisible(true);
	Objects[17].SetProperty(PROPERTY::STOP);

	Objects[18].SetKind(OBJECT_KIND::WALL);
	a = 12, b = 9;
	Objects[18].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[18].SetSize(BLOCK_SIZE);
	Objects[18].SetVisible(true);
	Objects[18].SetProperty(PROPERTY::STOP);

	Objects[19].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 9;
	Objects[19].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[19].SetSize(BLOCK_SIZE);
	Objects[19].SetVisible(true);
	Objects[19].SetProperty(PROPERTY::STOP);

	Objects[20].SetKind(OBJECT_KIND::WALL);
	a = 19, b = 9;
	Objects[20].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[20].SetSize(BLOCK_SIZE);
	Objects[20].SetVisible(true);
	Objects[20].SetProperty(PROPERTY::STOP);

	Objects[21].SetKind(OBJECT_KIND::WALL);
	a = 22, b = 9;
	Objects[21].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[21].SetSize(BLOCK_SIZE);
	Objects[21].SetVisible(true);
	Objects[21].SetProperty(PROPERTY::STOP);

	Objects[22].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 12;
	Objects[22].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[22].SetSize(BLOCK_SIZE);
	Objects[22].SetVisible(true);
	Objects[22].SetProperty(PROPERTY::STOP);

	Objects[23].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 14;
	Objects[23].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[23].SetSize(BLOCK_SIZE);
	Objects[23].SetVisible(true);
	Objects[23].SetProperty(PROPERTY::STOP);

	Objects[24].SetKind(OBJECT_KIND::WALL);
	a = 14, b = 13;
	Objects[24].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[24].SetSize(BLOCK_SIZE);
	Objects[24].SetVisible(true);
	Objects[24].SetProperty(PROPERTY::STOP);

	Objects[25].SetKind(OBJECT_KIND::WALL);
	a = 9, b = 16;
	Objects[25].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[25].SetSize(BLOCK_SIZE);
	Objects[25].SetVisible(true);
	Objects[25].SetProperty(PROPERTY::STOP);

	Objects[26].SetKind(OBJECT_KIND::WALL);
	a = 16, b = 16;
	Objects[26].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[26].SetSize(BLOCK_SIZE);
	Objects[26].SetVisible(true);
	Objects[26].SetProperty(PROPERTY::STOP);

	Objects[27].SetKind(OBJECT_KIND::WALL);
	a = 22, b = 16;
	Objects[27].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[27].SetSize(BLOCK_SIZE);
	Objects[27].SetVisible(true);
	Objects[27].SetProperty(PROPERTY::STOP);

	for (auto idx = 28; idx < 34; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 15][3].GetPosition().x, GridArray[idx - 15][3].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 34; idx < 36; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 24][9].GetPosition().x, GridArray[idx - 24][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 36; idx < 38; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 19][9].GetPosition().x, GridArray[idx - 19][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 38; idx < 40; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 18][9].GetPosition().x, GridArray[idx - 18][9].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 40; idx < 46; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 30][16].GetPosition().x, GridArray[idx - 30][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 46; idx < 51; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 29][16].GetPosition().x, GridArray[idx - 29][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 51; idx < 56; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[12][idx - 47].GetPosition().x, GridArray[12][idx - 47].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 56; idx < 61; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[19][idx - 52].GetPosition().x, GridArray[19][idx - 52].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 61; idx < 67; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[9][idx - 51].GetPosition().x, GridArray[9][idx - 51].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 67; idx < 73; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[22][idx - 57].GetPosition().x, GridArray[22][idx - 57].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[73].SetKind(OBJECT_KIND::WALL);
	Objects[73].SetPosition(GridArray[16][15].GetPosition().x, GridArray[16][15].GetPosition().y);
	Objects[73].SetSize(BLOCK_SIZE);
	Objects[73].SetVisible(true);
	Objects[73].SetProperty(PROPERTY::STOP);

	for (auto idx = 74; idx < 76; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[16][idx - 64].GetPosition().x, GridArray[16][idx - 64].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[76].SetKind(OBJECT_KIND::ROCK);
	Objects[76].SetPosition(GridArray[17][7].GetPosition().x, GridArray[17][7].GetPosition().y);
	Objects[76].SetSize(BLOCK_SIZE);
	Objects[76].SetVisible(true);
	Objects[76].SetProperty(PROPERTY::PUSH);

	Objects[77].SetKind(OBJECT_KIND::ROCK);
	Objects[77].SetPosition(GridArray[20][12].GetPosition().x, GridArray[20][12].GetPosition().y);
	Objects[77].SetSize(BLOCK_SIZE);
	Objects[77].SetVisible(true);
	Objects[77].SetProperty(PROPERTY::PUSH);

	for (auto idx = 78; idx < 83; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[7][idx - 78].GetPosition().x, GridArray[7][idx - 78].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[83].SetKind(OBJECT_KIND::WALL);
	Objects[83].SetPosition(GridArray[7][5].GetPosition().x, GridArray[7][5].GetPosition().y);
	Objects[83].SetSize(BLOCK_SIZE);
	Objects[83].SetVisible(true);
	Objects[83].SetProperty(PROPERTY::STOP);

	for (auto idx = 84; idx < 91; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::WALL);
		Objects[idx].SetPosition(GridArray[idx - 84][5].GetPosition().x, GridArray[idx - 84][5].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 91; idx < 98; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 91][0].GetPosition().x, GridArray[idx - 91][0].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 98; idx < 105; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 98][1].GetPosition().x, GridArray[idx - 98][1].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 105; idx < 110; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 105][2].GetPosition().x, GridArray[idx - 105][2].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 110; idx < 115; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 110][3].GetPosition().x, GridArray[idx - 110][3].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (auto idx = 115; idx < 120; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLOWER);
		Objects[idx].SetPosition(GridArray[idx - 115][4].GetPosition().x, GridArray[idx - 115][4].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Objects[120].SetKind(OBJECT_KIND::WATER);
	Objects[120].SetPosition(GridArray[15][10].GetPosition().x, GridArray[15][10].GetPosition().y);
	Objects[120].SetSize(BLOCK_SIZE);
	Objects[120].SetVisible(true);
	Objects[120].SetProperty(PROPERTY::OVERRIDE);

	Objects[121].SetKind(OBJECT_KIND::WATER);
	Objects[121].SetPosition(GridArray[10][13].GetPosition().x, GridArray[10][13].GetPosition().y);
	Objects[121].SetSize(BLOCK_SIZE);
	Objects[121].SetVisible(true);
	Objects[121].SetProperty(PROPERTY::OVERRIDE);

	Objects[122].SetKind(OBJECT_KIND::WATER);
	Objects[122].SetPosition(GridArray[12][13].GetPosition().x, GridArray[12][13].GetPosition().y);
	Objects[122].SetSize(BLOCK_SIZE);
	Objects[122].SetVisible(true);
	Objects[122].SetProperty(PROPERTY::OVERRIDE);

	Objects[123].SetKind(OBJECT_KIND::FLAG); // 플래그
	Objects[123].SetPosition(GridArray[10][15].GetPosition().x, GridArray[10][15].GetPosition().y);
	Objects[123].SetSize(BLOCK_SIZE);
	Objects[123].SetVisible(true);
	Objects[123].SetProperty(PROPERTY::OVERRIDE);

	Objects[124].SetKind(OBJECT_KIND::WATER);
	Objects[124].SetPosition(GridArray[12][15].GetPosition().x, GridArray[12][15].GetPosition().y);
	Objects[124].SetSize(BLOCK_SIZE);
	Objects[124].SetVisible(true);
	Objects[124].SetProperty(PROPERTY::OVERRIDE);

	Objects[125].SetKind(OBJECT_KIND::WATER);
	Objects[125].SetPosition(GridArray[11][13].GetPosition().x, GridArray[11][13].GetPosition().y);
	Objects[125].SetSize(BLOCK_SIZE);
	Objects[125].SetVisible(true);
	Objects[125].SetProperty(PROPERTY::OVERRIDE);

	Objects[126].SetKind(OBJECT_KIND::WATER);
	Objects[126].SetPosition(GridArray[11][14].GetPosition().x, GridArray[11][14].GetPosition().y);
	Objects[126].SetSize(BLOCK_SIZE);
	Objects[126].SetVisible(true);
	Objects[126].SetProperty(PROPERTY::OVERRIDE);

	Objects[127].SetKind(OBJECT_KIND::WATER);
	Objects[127].SetPosition(GridArray[11][15].GetPosition().x, GridArray[11][15].GetPosition().y);
	Objects[127].SetSize(BLOCK_SIZE);
	Objects[127].SetVisible(true);
	Objects[127].SetProperty(PROPERTY::OVERRIDE);

	Objects[128].SetKind(OBJECT_KIND::WATER);
	Objects[128].SetPosition(GridArray[10][14].GetPosition().x, GridArray[10][14].GetPosition().y);
	Objects[128].SetSize(BLOCK_SIZE);
	Objects[128].SetVisible(true);
	Objects[128].SetProperty(PROPERTY::OVERRIDE);

	Objects[129].SetKind(OBJECT_KIND::WATER);
	Objects[129].SetPosition(GridArray[12][14].GetPosition().x, GridArray[12][14].GetPosition().y);
	Objects[129].SetSize(BLOCK_SIZE);
	Objects[129].SetVisible(true);
	Objects[129].SetProperty(PROPERTY::OVERRIDE);

	Stage03_isExit = false;
	Stage03_isFirst = true;
	Stage03_babaIsDead = false;
	Stage03_babaIsWin = false;
}

void SceneStage03::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);

	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			if (*m_CurrentGridMode)
			{
				hPen = CreatePen(PS_SOLID, 1, RGB(51, 51, 51));
				hOldPen = (HPEN)SelectObject(_hdc, hPen);
				hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				hOldBrush = (HBRUSH)SelectObject(_hdc, hBrush);

				Rectangle(_hdc, GridArray[garo][sero].GetPosition().x - (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y - (GridArray[garo][sero].GetSize().y / 2),
					GridArray[garo][sero].GetPosition().x + (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y + (GridArray[garo][sero].GetSize().y / 2));

				SelectObject(_hdc, hOldBrush);
				DeleteObject(hBrush);
				SelectObject(_hdc, hOldPen);
				DeleteObject(hPen);
			}
		}
	}

	for (auto& v : tiles)
	{
		v->Render(_hdc, _hWnd);
	}

	for (auto& v2 : grass_tiles)
	{
		v2->Render(_hdc, _hWnd);
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Render(_hdc, _hWnd);
	}

	m_Player->Render(_hdc, _hWnd);

	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneStage03::Update(HWND _hWnd)
{
	if (m_Player->IsLife() == false)
	{
		m_Timer++; // 제한시간 안에 다시 원래대로 안돌리면 계속 증가함.
	}

	WinCheck(_hWnd);
	UpdateObjectFeature(_hWnd);

	m_BackgroundImage->Update(_hWnd);

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Update(_hWnd);
	}

	m_Player->Update(_hWnd, Objects);

	if (Stage03_isExit)
	{
		if (Stage03_babaIsWin == true)
		{
			m_FadeOut = true;
			m_NextGameState = STAGE_SELECT;
		}
		else if (Stage03_babaIsDead == true)
		{
			Init(m_hInstance);
		}
	}

	FadeUpdate();
}

void SceneStage03::SpriteUpdate(HWND _hWnd)
{
	for (auto& v : tiles)
	{
		v->Update(_hWnd);
	}

	for (auto& v2 : grass_tiles)
	{
		v2->Update(_hWnd);
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SpriteUpdate(_hWnd);
	}

	m_Player->SpriteUpdate(_hWnd);
}

void SceneStage03::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		break;
	case WM_MOUSEMOVE:
		break;
	}
}

void SceneStage03::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			m_FadeOut = true;
			m_NextGameState = SETTING;
		}
		break;
		case VK_RETURN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
		}
		break;
		case VK_UP:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_DOWN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y + BLOCK_SIZE > (WINDOW_SIZE_Y - 39))
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_LEFT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_RIGHT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x + BLOCK_SIZE > WINDOW_SIZE_X)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		}
		break;
	}
}

void SceneStage03::StopAll()
{
	FadeStopAll();

	if (*m_CurrentGameState != SETTING)
		m_Sound->StopSound();
}

void SceneStage03::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneStage03::GetStageState(bool * state, int size)
{
	for (int idx = 0; idx < size; idx++)
	{
		m_StageState[idx] = state[idx];
	}
}

void SceneStage03::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

const bool SceneStage03::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneStage03::GetGridMode(bool * value)
{
	m_CurrentGridMode = value;
}

void SceneStage03::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneStage03::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneStage03::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{
	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneStage03::FadeIn()
{
	m_alpha -= ALPHA_SPEED;
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneStage03::FadeOut()
{
	m_alpha += ALPHA_SPEED;
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}

void SceneStage03::FadeStopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;
}

void SceneStage03::UpdateObjectFeature(HWND hWnd)
{
	if (Objects.size() != 0)
	{
		for (auto& v : Objects)
		{
			///////////////////////////////////////////////////////////////////////////////// PUSH 기준
			GameObject* target = &Objects[11];

			POINT targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			GameObject* targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			GameObject* targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
			}


			target = &Objects[11];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
			}


			if ((targetObj1 != nullptr && targetObj2 == nullptr) || (targetObj1 == nullptr && targetObj2 != nullptr) || (targetObj1 == nullptr && targetObj2 == nullptr))
			{
				for (auto& v2 : Objects)
				{
					if (v2.GetKind() == FLAG || v2.GetKind() == ROCK)
					{
						v2.SetProperty(PROPERTY::OVERRIDE);
					}
				}
			}


			if (v.GetKind() == WATER)
			{
				if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 패배
				{
					m_Player->SetLife(false);
					m_Player->SetCanMove(false);
				}
			}

			BabaIsDead(hWnd);

			///////////////////////////////////////////////////////////////////////////////// WIN 기준
			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::FLAG_IS_WIN;
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == ROCK)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::ROCK_IS_WIN;
				}
			}


			///////////////////////////////////////////////////////////////////////////////// WIN 기준
			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::FLAG_IS_WIN;
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == ROCK)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::ROCK_IS_WIN;
				}
			}


			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);
			GameObject* targetObj3 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			GameObject* targetObj4 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if ((targetObj1 == nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
			else if ((targetObj1 != nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
			else if ((targetObj1 == nullptr && targetObj2 != nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
		}
	}
}

void SceneStage03::WinCheck(HWND hWnd)
{
	switch (m_WinCondition)
	{
	case FLAG_IS_WIN:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == FLAG)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						BabaIsWin(hWnd);
						m_PuzzleCompleted = true;
					}
				}
			}
		}
	}
	break;
	case ROCK_IS_WIN:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == ROCK)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						BabaIsWin(hWnd);
						m_PuzzleCompleted = true;
					}
				}
			}
		}
	}
	break;
	}
}

void SceneStage03::BabaIsDead(HWND hWnd)
{
	if (m_Timer > 60)
	{
		if (Stage03_isFirst)
		{
			Stage03_babaIsDead = true;
			Stage03_babaIsWin = false;
			Stage03_isFirst = false;
			m_DefeatSound->PlaySound(false, 1.0f);
			DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
		}

		m_Timer = 0;
	}
}

void SceneStage03::BabaIsWin(HWND hWnd)
{
	m_Player->SetLife(true);
	m_Player->SetCanMove(false);

	if (Stage03_isFirst)
	{
		Stage03_babaIsDead = false;
		Stage03_babaIsWin = true;
		Stage03_isFirst = false;
		m_Sound->StopSound();
		m_CompleteSound->PlaySound(false, 1.0f);
		DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
	}
}

const bool SceneStage03::GetPuzzleCompleted() const
{
	return m_PuzzleCompleted;
}

BOOL CALLBACK SceneStage03::DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hDialogDC, hDialogMemDC, hDialogBitDC;
	static RECT DialogCRT; // 직사각형모양 범 위 선언
	PAINTSTRUCT DialogPS;
	static HBITMAP hDialogBitmap, hDialogOldBitmap;

	static HBITMAP hButtonBitmap;

	static HWND _hWnd;

	static ObjectImage* DialogBackground;
	static 	ObjectImage* DialogNotifyDefeatImage;
	static 	ObjectImage* DialogNotifyConguratulationImage;
	static 	ObjectImage* DialogSpaceImage;
	static 	ObjectImage* DialogEnterImage;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		_hWnd = (HWND)GetWindowLongPtrA(hWnd, GWLP_HWNDPARENT);
		GetClientRect(hWnd, &DialogCRT);

		DialogBackground = new ObjectImage();
		DialogBackground->LoadSpriteImage(L"Data/Images/Title/TitleBackground.png", 792, 432, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2), DialogCRT.right, DialogCRT.bottom);

		DialogNotifyDefeatImage = new ObjectImage();
		DialogNotifyDefeatImage->LoadSpriteImage(L"Data/Images/Notify_Defeat.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogNotifyConguratulationImage = new ObjectImage();
		DialogNotifyConguratulationImage->LoadSpriteImage(L"Data/Images/Notify_Congratulation.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogSpaceImage = new ObjectImage();
		DialogSpaceImage->LoadSpriteImage(L"Data/Images/Space.png", 103, 50, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 25, 70, 50);

		DialogEnterImage = new ObjectImage();
		DialogEnterImage->LoadSpriteImage(L"Data/Images/Map/Enter.png", 48, 24, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 75, 70, 50);

		SetTimer(hWnd, 3, 1, NULL);
		SetTimer(hWnd, 4, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 3:
		{
			if (GetAsyncKeyState(VK_RETURN) & 0x8000)
			{
				Stage03_isExit = true;

				delete DialogBackground;
				delete DialogNotifyDefeatImage;
				delete DialogNotifyConguratulationImage;
				delete DialogSpaceImage;
				delete DialogEnterImage;

				EndDialog(hWnd, 0);
			}
		}
		break;
		case 4:
		{
			if (DialogBackground)
				DialogBackground->Update(hWnd);

			if (DialogNotifyDefeatImage)
				DialogNotifyDefeatImage->Update(hWnd);

			if (DialogNotifyConguratulationImage)
				DialogNotifyConguratulationImage->Update(hWnd);

			if (DialogSpaceImage)
				DialogSpaceImage->Update(hWnd);

			if (DialogEnterImage)
				DialogEnterImage->Update(hWnd);
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hDialogDC = BeginPaint(hWnd, &DialogPS);

		hDialogMemDC = CreateCompatibleDC(hDialogDC);
		hDialogBitmap = CreateCompatibleBitmap(hDialogDC, DialogCRT.right, DialogCRT.bottom);
		hDialogOldBitmap = (HBITMAP)SelectObject(hDialogMemDC, hDialogBitmap);
		FillRect(hDialogMemDC, &DialogCRT, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		if (DialogBackground)
			DialogBackground->Render(hDialogMemDC, hWnd);

		// 패배일 시
		if (DialogNotifyDefeatImage && Stage03_babaIsDead)
			DialogNotifyDefeatImage->Render(hDialogMemDC, hWnd);

		// 성공일 시
		if (DialogNotifyConguratulationImage && Stage03_babaIsWin)
			DialogNotifyConguratulationImage->Render(hDialogMemDC, hWnd);

		if (DialogSpaceImage)
			DialogSpaceImage->Render(hDialogMemDC, hWnd);

		if (DialogEnterImage)
			DialogEnterImage->Render(hDialogMemDC, hWnd);

		BitBlt(hDialogDC, 0, 0, DialogCRT.right, DialogCRT.bottom, hDialogMemDC, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(hDialogMemDC, hDialogOldBitmap));
		DeleteDC(hDialogMemDC);

		EndPaint(hWnd, &DialogPS);
	}
	break;
	}
	return 0;
}