#include "stdafx.h"
#include "GameFramework.h"
#include "SceneTitle.h"
#include "SceneSelect.h"
#include "SceneTutorial.h"
#include "SceneSettings.h"
#include "SceneStage01.h"
#include "SceneStage02.h"
#include "SceneStage03.h"
#include "SceneStage04.h"

GameFramework::GameFramework()
{
	m_SceneTitle = new SceneTitle();
	m_SceneTitle->GetGameState(&m_GameState);

	m_SceneSelect = new SceneSelect();
	m_SceneSelect->GetGameState(&m_GameState);
	m_SceneSelect->GetGridMode(&m_GridState);

	m_SceneTutorial = new SceneTutorial();
	m_SceneTutorial->GetGameState(&m_GameState);
	m_SceneTutorial->GetGridMode(&m_GridState);
	m_SceneTutorial->SetCanEnter(true);

	m_SceneSettings = new SceneSettings();
	m_SceneSettings->GetGameState(&m_GameState);
	m_SceneSettings->GetGridMode(&m_GridState);

	m_SceneStage01 = new SceneStage01();
	m_SceneStage01->GetGameState(&m_GameState);
	m_SceneStage01->GetGridMode(&m_GridState);
	m_SceneStage01->SetCanEnter(true);

	m_SceneStage02 = new SceneStage02();
	m_SceneStage02->GetGameState(&m_GameState);
	m_SceneStage02->GetGridMode(&m_GridState);
	m_SceneStage02->SetCanEnter(true);

	m_SceneStage03 = new SceneStage03();
	m_SceneStage03->GetGameState(&m_GameState);
	m_SceneStage03->GetGridMode(&m_GridState);
	m_SceneStage03->SetCanEnter(true);

	m_SceneStage04 = new SceneStage04();
	m_SceneStage04->GetGameState(&m_GameState);
	m_SceneStage04->GetGridMode(&m_GridState);
	m_SceneStage04->SetCanEnter(true);
}

GameFramework::~GameFramework()
{
	if (m_SceneTitle)
		delete m_SceneTitle;

	if (m_SceneSelect)
		delete m_SceneSelect;

	if (m_SceneTutorial)
		delete m_SceneTutorial;

	if (m_SceneSettings)
		delete m_SceneSettings;

	if (m_SceneStage01)
		delete m_SceneStage01;

	if (m_SceneStage02)
		delete m_SceneStage02;

	if (m_SceneStage03)
		delete m_SceneStage03;

	if (m_SceneStage04)
		delete m_SceneStage04;
}

void GameFramework::Init(HINSTANCE _hInstance, HWND _hWnd)
{
	m_hInstance = _hInstance;
	m_hWnd = _hWnd;
}

void GameFramework::Render(HDC _hdc, HWND _hWnd)
{
	SetBkMode(_hdc, TRANSPARENT);

	switch (m_GameState)
	{
	case TITLE:
		m_SceneTitle->Render(_hdc, _hWnd);
		break;
	case STAGE_SELECT:
		m_SceneSelect->Render(_hdc, _hWnd);
		break;
	case TUTORIAL:
		m_SceneTutorial->Render(_hdc, _hWnd);
		break;
	case SETTING:
		m_SceneSettings->Render(_hdc, _hWnd);
		break;
	case STAGE1:
		m_SceneStage01->Render(_hdc, _hWnd);
		break;
	case STAGE2:
		m_SceneStage02->Render(_hdc, _hWnd);
		break;
	case STAGE3:
		m_SceneStage03->Render(_hdc, _hWnd);
		break;
	case STAGE4:
		 m_SceneStage04->Render(_hdc, _hWnd);
		break;
	}
}

void GameFramework::Update(HWND _hWnd)
{
	if (m_SceneTutorial->GetPuzzleCompleted())
		m_StageState[STAGE1] = true;

	if (m_SceneStage01->GetPuzzleCompleted())
		m_StageState[STAGE2] = true;

	if (m_SceneStage02->GetPuzzleCompleted())
		m_StageState[STAGE3] = true;

	if (m_SceneStage03->GetPuzzleCompleted())
		m_StageState[STAGE4] = true;

	m_SceneSelect->GetStageState(m_StageState, 7);
	m_SceneTutorial->GetStageState(m_StageState, 7);
	m_SceneStage01->GetStageState(m_StageState, 7);
	m_SceneStage02->GetStageState(m_StageState, 7);
	m_SceneStage03->GetStageState(m_StageState, 7);
	 m_SceneStage04->GetStageState(m_StageState, 7);

	if (m_PrevGameState != m_GameState)
	{
		switch (m_PrevGameState) // 장면 전환되기 전에 모든 것을 중지
		{
		case TITLE:
			m_SceneTitle->StopAll();
			break;
		case STAGE_SELECT:
			m_SceneSelect->StopAll();
			break;
		case TUTORIAL:
			m_SceneTutorial->GetGameState(&m_GameState);
			m_SceneTutorial->StopAll();
			break;
		case SETTING:
			m_SceneSettings->GetGameState(&m_GameState);
			m_SceneSettings->StopAll();
			break;
		case STAGE1:
			m_SceneStage01->GetGameState(&m_GameState);
			m_SceneStage01->StopAll();
			break;
		case STAGE2:
			m_SceneStage02->GetGameState(&m_GameState);
			m_SceneStage02->StopAll();
			break;
		case STAGE3:
			 m_SceneStage03->GetGameState(&m_GameState);
			 m_SceneStage03->StopAll();
			break;
		case STAGE4:
			 m_SceneStage04->GetGameState(&m_GameState);
			 m_SceneStage04->StopAll();
			break;
		}

		switch (m_GameState) // 장면 전환된 후에 초기화
		{
		case TITLE:
			if(m_SceneTitle->GetCanEnter() == true)
				m_SceneTitle->Init();

			m_SceneSelect->StopAll();
			m_SceneTutorial->StopAll();
			m_SceneSettings->StopAll();
			m_SceneStage01->StopAll();
			m_SceneStage02->StopAll();
			m_SceneStage03->StopAll();
			 m_SceneStage04->StopAll();
			break;
		case STAGE_SELECT:
			if (m_SceneSelect->GetCanEnter() == true)
			{
				m_SceneSelect->GetGridMode(&m_GridState);
				m_SceneSelect->Init(m_hInstance);
			}
			break;
		case TUTORIAL:
			if (m_SceneTutorial->GetCanEnter() == true)
			{
				m_SceneTutorial->Init(m_hInstance);
				m_SceneTutorial->GetGridMode(&m_GridState);
			}
			break;
		case SETTING:
			if (m_SceneSettings->GetCanEnter() == true)
			{
				m_SceneSettings->SetPreviousGameState(m_PrevGameState);
				m_SceneSettings->Init();
			}
			break;
		case STAGE1:
			if (m_SceneStage01->GetCanEnter() == true)
			{
				m_SceneStage01->Init(m_hInstance);
				m_SceneStage01->GetGridMode(&m_GridState);
			}
			break;
		case STAGE2:
			if (m_SceneStage02->GetCanEnter() == true)
			{
				m_SceneStage02->Init(m_hInstance);
				m_SceneStage02->GetGridMode(&m_GridState);
			}
			break;
		case STAGE3:
			if (m_SceneStage03->GetCanEnter() == true)
			{
				m_SceneStage03->Init(m_hInstance);
				m_SceneStage03->GetGridMode(&m_GridState);
			}
			break;
		case STAGE4:
			if (m_SceneStage04->GetCanEnter() == true)
			{
				m_SceneStage04->Init(m_hInstance);
				m_SceneStage04->GetGridMode(&m_GridState);
			}
			break;
		}
	}

	m_PrevGameState = m_GameState;

	switch (m_GameState) // 업데이트
	{
	case TITLE:
		if (m_SceneTitle->GetCanEnter() == true)
			m_SceneTitle->Update(_hWnd);
		break;
	case STAGE_SELECT:
		if (m_SceneSelect->GetCanEnter() == true)
			m_SceneSelect->Update(_hWnd);
		break;
	case TUTORIAL:
		if (m_SceneTutorial->GetCanEnter() == true)
			m_SceneTutorial->Update(_hWnd);
		break;
	case SETTING:
		if (m_SceneSettings->GetCanEnter() == true)
			m_SceneSettings->Update(_hWnd);
		break;
	case STAGE1:
		if (m_SceneStage01->GetCanEnter() == true)
			m_SceneStage01->Update(_hWnd);
		break;
	case STAGE2:
		if (m_SceneStage02->GetCanEnter() == true)
			m_SceneStage02->Update(_hWnd);
		break;
	case STAGE3:
		if (m_SceneStage03->GetCanEnter() == true)
			m_SceneStage03->Update(_hWnd);
		break;
	case STAGE4:
		if (m_SceneStage04->GetCanEnter() == true)
			m_SceneStage04->Update(_hWnd);
		break;
	}
}

void GameFramework::SpriteUpdate(HWND _hWnd)
{
	switch (m_GameState) // 업데이트
	{
	case TITLE:
		if (m_SceneTitle->GetCanEnter() == true)
			m_SceneTitle->SpriteUpdate(_hWnd);
		break;
	case STAGE_SELECT:
		if (m_SceneSelect->GetCanEnter() == true)
			m_SceneSelect->SpriteUpdate(_hWnd);
		break;
	case TUTORIAL:
		if (m_SceneTutorial->GetCanEnter() == true)
			m_SceneTutorial->SpriteUpdate(_hWnd);
		break;
	case SETTING:
		if (m_SceneSettings->GetCanEnter() == true)
			m_SceneSettings->SpriteUpdate(_hWnd);
		break;
	case STAGE1:
		if (m_SceneStage01->GetCanEnter() == true)
			m_SceneStage01->SpriteUpdate(_hWnd);
		break;
	case STAGE2:
		if (m_SceneStage02->GetCanEnter() == true)
			m_SceneStage02->SpriteUpdate(_hWnd);
		break;
	case STAGE3:
		if (m_SceneStage03->GetCanEnter() == true)
			m_SceneStage03->SpriteUpdate(_hWnd);
		break;
	case STAGE4:
		if (m_SceneStage04->GetCanEnter() == true)
			m_SceneStage04->SpriteUpdate(_hWnd);
		break;
	}
}

void GameFramework::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (m_GameState)
	{
	case TITLE:
		m_SceneTitle->MouseMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE_SELECT:
		// 맵 선택화면에서는 마우스 메시지를 받지 않는다.
		break;
	case TUTORIAL:
		// 인게임화면에서는 마우스 메시지를 받지 않는다.
		break;
	case SETTING:
		m_SceneSettings->MouseMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE1:
		// 인게임화면에서는 마우스 메시지를 받지 않는다.
		break;
	case STAGE2:
		// 인게임화면에서는 마우스 메시지를 받지 않는다.
		break;
	case STAGE3:
		// 인게임화면에서는 마우스 메시지를 받지 않는다.
		break;
	case STAGE4:
		// 인게임화면에서는 마우스 메시지를 받지 않는다.
		break;
	}
}

void GameFramework::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (m_GameState)
	{
	case TITLE:
		m_SceneTitle->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE_SELECT:
		m_SceneSelect->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case TUTORIAL:
		m_SceneTutorial->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case SETTING:
		m_SceneSettings->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE1:
		m_SceneStage01->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE2:
		m_SceneStage02->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE3:
		m_SceneStage03->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	case STAGE4:
		m_SceneStage04->KeyboardMessage(hWnd, message, wParam, lParam);
		break;
	}
}

void GameFramework::SetGameState(int state)
{
	m_GameState = state;
}

const int GameFramework::GetCurrentState()
{
	return m_GameState;
}

