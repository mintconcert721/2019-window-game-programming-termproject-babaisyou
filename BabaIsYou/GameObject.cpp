#include "stdafx.h"
#include "GameObject.h"
#include "Baba.h"

GameObject::GameObject()
{
	m_Position.x = 0;
	m_Position.y = 0;
	m_Size.x = 0;
	m_Size.y = 0;

	m_Visible = false;
	m_Image = new ObjectImage();

	m_Velocity = 10;
}

GameObject::GameObject(int positionX, int positionY, int sizeX, int sizeY)
{
	m_Position.x = positionX;
	m_Position.y = positionY;
	m_Size.x = sizeX;
	m_Size.y = sizeY;

	m_Visible = false;
	m_Image = new ObjectImage();

	m_Velocity = 10;
}

GameObject::GameObject(const GameObject & rhs)
{
	m_Position.x = rhs.m_Position.x;
	m_Position.y = rhs.m_Position.y;
	m_Size.x = rhs.m_Size.x;
	m_Size.y = rhs.m_Size.y;

	m_Visible = rhs.m_Visible;
	
	if (m_Image != nullptr)
		delete m_Image;

	m_Image = new ObjectImage();
	memcpy_s(m_Image, sizeof(ObjectImage), rhs.m_Image, sizeof(ObjectImage));

	m_Velocity = rhs.m_Velocity;
}

GameObject::~GameObject()
{
	delete m_Image;
}

void GameObject::Init()
{
}

void GameObject::Update(HWND _hWnd)
{
	if (m_Visible)
	{
		if (m_IsPawn) // 캐릭터가 빙의된 것이면, 움직일 수 있다
		{
			if (m_MyState == OBJECTSTATE::STOPPED)
			{

			}
			else if (m_MyState == OBJECTSTATE::MOVING && m_Property == PROPERTY::PUSH)
			{
				if (m_MoveTimer <= 0)
				{
					m_MoveTimer = 0;
					m_MyState = OBJECTSTATE::STOPPED;
				}
				else
				{
					switch (m_Direction)
					{
					case OBJECTDIRECTION::NONE:
					{

					}
					break;
					case OBJECTDIRECTION::UP:
					{
						m_Position.y = m_Position.y - m_Velocity;
					}
					break;
					case OBJECTDIRECTION::DOWN:
					{
						m_Position.y = m_Position.y + m_Velocity;
					}
					break;
					case OBJECTDIRECTION::LEFT:
					{
						m_Position.x = m_Position.x - m_Velocity;
					}
					break;
					case OBJECTDIRECTION::RIGHT:
					{
						m_Position.x = m_Position.x + m_Velocity;
					}
					break;
					}
				}

				m_MoveTimer -= m_Velocity;
			}
			m_Image->SetPosition(m_Position.x, m_Position.y);
		}
		else
		{
			if (m_MyState == OBJECTSTATE::MOVING)
			{
				if (m_MoveTimer <= 0)
				{
					m_MoveTimer = 0;
					m_MyState = OBJECTSTATE::STOPPED;
					m_VariationIsFinished = true;
				}
				else
				{
					switch (m_Direction)
					{
					case OBJECTDIRECTION::NONE:
					{

					}
					break;
					case OBJECTDIRECTION::UP:
					{
						m_Position.y = m_Position.y - m_Velocity;
					}
					break;
					case OBJECTDIRECTION::DOWN:
					{
						m_Position.y = m_Position.y + m_Velocity;
					}
					break;
					case OBJECTDIRECTION::LEFT:
					{
						m_Position.x = m_Position.x - m_Velocity;
					}
					break;
					case OBJECTDIRECTION::RIGHT:
					{
						m_Position.x = m_Position.x + m_Velocity;
					}
					break;
					}
				}

				m_MoveTimer -= m_Velocity;
			}
			m_Image->SetPosition(m_Position.x, m_Position.y);
		}
	}
}

void GameObject::SpriteUpdate(HWND _hWnd)
{
	if (m_Visible)
	{
		m_Image->Update(_hWnd);
	}
}

void GameObject::Render(HDC _hdc, HWND _hWnd)
{
	if(m_Visible)
		m_Image->Render(_hdc, _hWnd);
}

void GameObject::SetImage(LPCTSTR fileName, int spriteGaroPixel, int spriteSeroPixel, int spriteEntireCount)
{
	m_Image->LoadSpriteImage(fileName, spriteGaroPixel, spriteSeroPixel, spriteEntireCount, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
}

void GameObject::SetPosition(int positionX, int positionY)
{
	m_Position.x = positionX;
	m_Position.y = positionY;
}

void GameObject::SetGoalPosition(int goalX, int goalY)
{
	m_GoalPosition.x = goalX;
	m_GoalPosition.y = goalY;
}

const POINT GameObject::GetPosition() const
{
	return m_Position;
}

void GameObject::SetSize(int sizeX, int sizeY)
{
	m_Size.x = sizeX;
	m_Size.y = sizeY;
}

void GameObject::SetSize(int size)
{
	m_Size.x = size;
	m_Size.y = size;
}

const POINT GameObject::GetSize() const
{
	return m_Size;
}

void GameObject::SetVisible(bool value)
{
	m_Visible = value;
}

const bool GameObject::GetVisible() const
{
	return m_Visible;
}

void GameObject::SetKind(int value)
{
	m_Kind = value;
}

const int GameObject::GetKind() const
{
	return m_Kind;
}

void GameObject::SetPawn(bool value)
{
	m_IsPawn = value;
}

const bool GameObject::GetPawn() const
{
	return m_IsPawn;
}

void GameObject::SetProperty(int value)
{
	m_Property = value;
}

const int GameObject::GetProperty() const
{
	return m_Property;
}

void GameObject::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_MyState == OBJECTSTATE::STOPPED)
	{
		switch (message)
		{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_UP:
			{
				if (m_Position.y - m_WalkingDistance < 0)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::UP;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_DOWN:
			{
				if (m_Position.y + m_WalkingDistance > (WINDOW_SIZE_Y - 39))
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::DOWN;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_LEFT:
			{
				if (m_Position.x - m_WalkingDistance < 0)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::LEFT;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_RIGHT:
			{
				if (m_Position.x + m_WalkingDistance > WINDOW_SIZE_X)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::RIGHT;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			}
		}
		break;
		}
	}
}

void GameObject::SetState(OBJECTSTATE state)
{
	m_MyState = state;
}

const OBJECTSTATE GameObject::GetState() const
{
	return m_MyState;
}

void GameObject::SetDirection(OBJECTDIRECTION direction)
{
	m_Direction = direction;
}

const OBJECTDIRECTION GameObject::GetDirection() const
{
	return m_Direction;
}

void GameObject::SetMoveTimer(int value)
{
	m_MoveTimer = value;
}

void GameObject::SetVariationIsFinished(bool value)
{
	m_VariationIsFinished = value;
}

const bool GameObject::GetVariationIsFinished() const
{
	return m_VariationIsFinished;
}

bool GameObject::IsPointInRect(POINT pt, POINT obj_pt)
{
	if ((obj_pt.x == pt.x) && (obj_pt.y == pt.y))
	{
		return true;
	}

	return false;
}

GameObject* GameObject::GetObjects(POINT coord, vector<GameObject>& container)
{
	for (auto& v : container)
	{
		if (IsPointInRect(coord, v.GetPosition()))
		{
			return &v;
		}
	}

	return nullptr;
}

bool GameObject::FindNearObjects(Baba* p, GameObject* rhs, vector<GameObject>& container, OBJECTSTATE obj_state, OBJECTDIRECTION obj_direction)
{
	bool res;

	if (rhs == nullptr)
	{
		res =  true;
		return res;
	}

	if (rhs->GetProperty() == PROPERTY::STOP)
	{
		for (auto& v : p->tempContainer)
		{
			v->SetState(OBJECTSTATE::STOPPED);
			v->SetDirection(OBJECTDIRECTION::NONE);
			v->SetMoveTimer(0);
			v->SetVariationIsFinished(true);
		}

		p->SetState(OBJECTSTATE::STOPPED);
		p->SetMoveTimer(0);
		p->m_VariationFinished = false;

		res = false;
		return res;
	}

	if (rhs->GetProperty() == PROPERTY::PUSH)
	{
		switch (obj_direction)
		{
		case UP:
		{
			if (rhs->GetPosition().y - BLOCK_SIZE < -20)
			{
				for (auto& v : p->tempContainer)
				{
					v->SetState(OBJECTSTATE::STOPPED);
					v->SetDirection(OBJECTDIRECTION::NONE);
					v->SetMoveTimer(0);
					v->SetVariationIsFinished(true);
				}

				p->SetState(OBJECTSTATE::STOPPED);
				p->SetMoveTimer(0);
				p->m_VariationFinished = false;

				res = false;
				return res;
			}
		}
		break;
		case DOWN:
		{
			if (rhs->GetPosition().y + rhs->m_WalkingDistance > (WINDOW_SIZE_Y - 19))
			{
				for (auto& v : p->tempContainer)
				{
					v->SetState(OBJECTSTATE::STOPPED);
					v->SetDirection(OBJECTDIRECTION::NONE);
					v->SetMoveTimer(0);
					v->SetVariationIsFinished(true);
				}

				p->SetState(OBJECTSTATE::STOPPED);
				p->SetMoveTimer(0);
				p->m_VariationFinished = false;

				res = false;
				return res;
			}
		}
		break;
		case LEFT:
		{
			if (rhs->GetPosition().x - BLOCK_SIZE < 0)
			{
				for (auto& v : p->tempContainer)
				{
					v->SetState(OBJECTSTATE::STOPPED);
					v->SetDirection(OBJECTDIRECTION::NONE);
					v->SetMoveTimer(0);
					v->SetVariationIsFinished(true);
				}

				p->SetState(OBJECTSTATE::STOPPED);
				p->SetMoveTimer(0);
				p->m_VariationFinished = false;

				res = false;
				return res;
			}
		}
		break;
		case RIGHT:
		{
			if (rhs->GetPosition().x + BLOCK_SIZE > WINDOW_SIZE_X)
			{
				for (auto& v : p->tempContainer)
				{
					v->SetState(OBJECTSTATE::STOPPED);
					v->SetDirection(OBJECTDIRECTION::NONE);
					v->SetMoveTimer(0);
					v->SetVariationIsFinished(true);
				}

				p->SetState(OBJECTSTATE::STOPPED);
				p->SetMoveTimer(0);
				p->m_VariationFinished = false;

				res = false;
				return res;
			}
		}
		break;
		}

		if (rhs->GetVariationIsFinished())
		{
			rhs->SetState(obj_state);
			rhs->SetDirection(obj_direction);
			rhs->SetMoveTimer(BLOCK_SIZE);
			rhs->SetVariationIsFinished(false);
		}

		p->tempContainer.insert(rhs);

		switch (obj_direction)
		{
		case UP:
			return rhs->FindNearObjects(p, rhs->GetObjects(POINT{ rhs->GetPosition().x, rhs->GetPosition().y - BLOCK_SIZE }, container), container, obj_state, obj_direction);
			break;
		case DOWN:
			return rhs->FindNearObjects(p, rhs->GetObjects(POINT{ rhs->GetPosition().x, rhs->GetPosition().y + BLOCK_SIZE }, container), container, obj_state, obj_direction);
			break;
		case LEFT:
			return rhs->FindNearObjects(p, rhs->GetObjects(POINT{ rhs->GetPosition().x - BLOCK_SIZE, rhs->GetPosition().y }, container), container, obj_state, obj_direction);
			break;
		case RIGHT:
			return rhs->FindNearObjects(p, rhs->GetObjects(POINT{ rhs->GetPosition().x + BLOCK_SIZE, rhs->GetPosition().y }, container), container, obj_state, obj_direction);
			break;
		}
	}
}

int GameObject::CollisionCheck(vector<GameObject>& container)
{
	for (auto idx = 0; idx < container.size(); idx++)
	{
		if (this == &container[idx])
			continue;

		if ((container[idx].m_Position.x == this->m_Position.x) &&
			(container[idx].m_Position.y == this->m_Position.y))
		{
			return container[idx].m_Kind;
		}
	}

	return -1;
}

bool GameObject::IsMouseCollision(POINT & mouseCoordinate)
{
	if (((m_Position.x - (m_Size.x / 2)) <= mouseCoordinate.x) && (mouseCoordinate.x <= (m_Position.x + (m_Size.x / 2))) &&
		((m_Position.y - (m_Size.y / 2)) <= mouseCoordinate.y) && (mouseCoordinate.y <= (m_Position.y + (m_Size.y / 2))))
	{
		return true;
	}

	return false;
}
