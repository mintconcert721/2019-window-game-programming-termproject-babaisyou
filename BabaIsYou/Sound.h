#pragma once

// #include <map>
// #include <iterator>
#include "System/include/irrKlang.h"
#pragma comment(lib, "System/lib/irrKlang.lib")

using namespace irrklang;

class Sound
{
private:
	ISoundEngine* m_Engine = NULL;
	ISoundSource* m_SoundSource = NULL;

	// map<int, ISoundSource*> m_SoundList;

public:
	// int CreateSound(char* filePath);
	// void DeleteSound(int index);
	// void PlaySound(int index, bool isLoop, float volume);

	void CreateSound(const char* filePath);
	void PlaySound(bool isLoop, float volume);
	void StopSound();

public:
	Sound();
	~Sound();
};

