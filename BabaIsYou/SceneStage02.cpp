#include "stdafx.h"
#include "resource.h"
#include "SceneStage02.h"
#include "Baba.h"

SceneStage02::SceneStage02()
{
	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			GridArray[garo][sero].SetPosition(garo * BLOCK_SIZE + (BLOCK_SIZE / 2), sero * BLOCK_SIZE + (BLOCK_SIZE / 2));
			GridArray[garo][sero].SetSize(BLOCK_SIZE);
			GridArray[garo][sero].SetKind(-1);
		}
	}

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Map/BG3.mp3");

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_CompleteSound = new Sound();
	m_CompleteSound->CreateSound("Data/Sounds/complete.ogg");

	m_DefeatSound = new Sound();
	m_DefeatSound->CreateSound("Data/Sounds/defeat.ogg");

	m_Player = new Baba();
	m_Player->SetKind(OBJECT_KIND::WALL);
	int px = 18, py = 13;
	m_Player->SetPosition(GridArray[px][py].GetPosition().x, GridArray[px][py].GetPosition().y);

	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Tutorial/TutorialBackground.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	int a, b;

	tiles.resize(15);
	for (auto idx = 0; idx < tiles.size(); idx++)
	{
		tiles[idx] = new ObjectImage();
		tiles[idx]->LoadSpriteImage(L"Data/Images/Tutorial/Tutorial_tile.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	}

	a = 3, b = 2;
	tiles[0]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 9, b = 1;
	tiles[1]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 6, b = 3;
	tiles[2]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 6, b = 4;
	tiles[3]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 3, b = 8;
	tiles[4]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 2, b = 9;
	tiles[5]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 5, b = 14;
	tiles[6]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 7, b = 16;
	tiles[7]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 21, b = 3;
	tiles[8]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 22, b = 4;
	tiles[9]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 29, b = 2;
	tiles[10]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 27, b = 10;
	tiles[11]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 24, b = 14;
	tiles[12]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 23, b = 15;
	tiles[13]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	a = 23, b = 16;
	tiles[14]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles.resize(12);
	for (auto idx = 0; idx < grass_tiles.size(); idx++)
	{
		grass_tiles[idx] = new ObjectImage();
	}

	grass_tiles[0]->LoadSpriteImage(L"Data/Images/Stage02/grass_9.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 10, b = 7;
	grass_tiles[0]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[1]->LoadSpriteImage(L"Data/Images/Stage02/grass_12.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 13, b = 7;
	grass_tiles[1]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[2]->LoadSpriteImage(L"Data/Images/Stage02/grass_3.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 10, b = 9;
	grass_tiles[2]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	grass_tiles[3]->LoadSpriteImage(L"Data/Images/Stage02/grass_6.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
	a = 13, b = 9;
	grass_tiles[3]->SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);

	for (auto idx = 4; idx < 6; idx++)
	{
		grass_tiles[idx]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
		grass_tiles[idx]->SetPosition(GridArray[idx + 7][7].GetPosition().x, GridArray[idx + 7][7].GetPosition().y);
	}

	for (auto idx = 6; idx < 10; idx++)
	{
		grass_tiles[idx]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
		grass_tiles[idx]->SetPosition(GridArray[idx + 4][8].GetPosition().x, GridArray[idx + 4][8].GetPosition().y);
	}

	for (auto idx = 10; idx < 12; idx++)
	{
		grass_tiles[idx]->LoadSpriteImage(L"Data/Images/Stage02/grass_7.png", 24, 24, 3, 0, 0, BLOCK_SIZE, BLOCK_SIZE);
		grass_tiles[idx]->SetPosition(GridArray[idx + 1][9].GetPosition().x, GridArray[idx + 1][9].GetPosition().y);
	}

	Objects.resize(10 + 56);
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SetKind(-1);
		Objects[idx].SetVisible(false);
	}

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 11, b = 8;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetImage(L"Data/Images/Map/text_baba.png", 24, 24, 3);
	Objects[0].SetProperty(PROPERTY::PUSH);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 15, b = 4;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[1].SetProperty(PROPERTY::PUSH);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 10, b = 14;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetImage(L"Data/Images/Map/text_you.png", 24, 24, 3);
	Objects[2].SetProperty(PROPERTY::PUSH);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 15, b = 12;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetImage(L"Data/Images/Map/text_flag.png", 24, 24, 3);
	Objects[3].SetProperty(PROPERTY::PUSH);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 15, b = 13;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[4].SetProperty(PROPERTY::PUSH);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 18, b = 6;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetImage(L"Data/Images/Map/text_win.png", 24, 24, 3);
	Objects[5].SetProperty(PROPERTY::PUSH);

	Objects[6].SetKind(TEXT_KIND::TEXT_WALL);
	a = 10, b = 12;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetImage(L"Data/Images/Tutorial/text_wall.png", 24, 24, 3);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 10, b = 13;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_STOP);
	a = 15, b = 14;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetImage(L"Data/Images/Tutorial/text_stop.png", 24, 24, 3);
	Objects[8].SetProperty(PROPERTY::PUSH);

	for (int idx = 9; idx < 17; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx + 4][2].GetPosition().x, GridArray[idx + 4][2].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 18; idx < 21; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[13][idx - 15].GetPosition().x, GridArray[13][idx - 15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 21; idx < 26; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 12][6].GetPosition().x, GridArray[idx - 12][6].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 26; idx < 29; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[9][idx - 19].GetPosition().x, GridArray[9][idx - 19].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 29; idx < 40; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 20][10].GetPosition().x, GridArray[idx - 20][10].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 40; idx < 45; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[13][idx - 29].GetPosition().x, GridArray[13][idx - 29].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 45; idx < 50; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 32][16].GetPosition().x, GridArray[idx - 32][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 50; idx < 53; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 33][15].GetPosition().x, GridArray[idx - 33][15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 53; idx < 66; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[20][idx - 50].GetPosition().x, GridArray[20][idx - 50].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}
}

SceneStage02::~SceneStage02()
{
	delete m_Player;

	delete m_BackgroundImage;
	delete m_FadeImage;

	for (auto& v : tiles)
	{
		if (v)
			delete v;
	}

	for (auto& v : grass_tiles)
	{
		if (v)
			delete v;
	}

	delete m_Sound;
	delete m_MoveSound;
	delete m_CompleteSound;
	delete m_DefeatSound;
}

void SceneStage02::Init(HINSTANCE _hInstance)
{
	m_hInstance = _hInstance;

	m_Sound->PlaySound(true, 0.1f);

	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;

	if (m_Player)
	{
		m_Player->SetPosition(GridArray[18][13].GetPosition().x, GridArray[18][13].GetPosition().y);
		m_Player->SetKind(OBJECT_KIND::WALL);
	}

	int a, b;

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 11, b = 8;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetProperty(PROPERTY::PUSH);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 15, b = 4;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetProperty(PROPERTY::PUSH);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 10, b = 14;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetProperty(PROPERTY::PUSH);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 15, b = 12;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetProperty(PROPERTY::PUSH);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 15, b = 13;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetProperty(PROPERTY::PUSH);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 18, b = 6;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetProperty(PROPERTY::PUSH);

	Objects[6].SetKind(TEXT_KIND::TEXT_WALL);
	a = 10, b = 12;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 10, b = 13;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_STOP);
	a = 15, b = 14;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetProperty(PROPERTY::PUSH);

	for (int idx = 9; idx < 17; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx + 4][2].GetPosition().x, GridArray[idx + 4][2].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 18; idx < 21; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[13][idx - 15].GetPosition().x, GridArray[13][idx - 15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 21; idx < 26; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 12][6].GetPosition().x, GridArray[idx - 12][6].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 26; idx < 29; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[9][idx - 19].GetPosition().x, GridArray[9][idx - 19].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 29; idx < 40; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 20][10].GetPosition().x, GridArray[idx - 20][10].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 40; idx < 45; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[13][idx - 29].GetPosition().x, GridArray[13][idx - 29].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 45; idx < 50; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 32][16].GetPosition().x, GridArray[idx - 32][16].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 50; idx < 53; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[idx - 33][15].GetPosition().x, GridArray[idx - 33][15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	for (int idx = 53; idx < 66; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::FLAG);
		Objects[idx].SetPosition(GridArray[20][idx - 50].GetPosition().x, GridArray[20][idx - 50].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::STOP);
	}

	Stage02_isExit = false;
	Stage02_isFirst = true;
	Stage02_babaIsDead = false;
	Stage02_babaIsWin = false;
	m_Player->SetCanMove(true);
}

void SceneStage02::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);

	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			if (*m_CurrentGridMode)
			{
				hPen = CreatePen(PS_SOLID, 1, RGB(51, 51, 51));
				hOldPen = (HPEN)SelectObject(_hdc, hPen);
				hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				hOldBrush = (HBRUSH)SelectObject(_hdc, hBrush);

				Rectangle(_hdc, GridArray[garo][sero].GetPosition().x - (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y - (GridArray[garo][sero].GetSize().y / 2),
					GridArray[garo][sero].GetPosition().x + (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y + (GridArray[garo][sero].GetSize().y / 2));

				SelectObject(_hdc, hOldBrush);
				DeleteObject(hBrush);
				SelectObject(_hdc, hOldPen);
				DeleteObject(hPen);
			}
		}
	}

	for (auto& v : tiles)
	{
		v->Render(_hdc, _hWnd);
	}

	for (auto& v2 : grass_tiles)
	{
		v2->Render(_hdc, _hWnd);
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Render(_hdc, _hWnd);
	}

	m_Player->Render(_hdc, _hWnd);

	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneStage02::Update(HWND _hWnd)
{
	if (m_Player->IsLife() == false)
	{
		m_Timer++; // 제한시간 안에 다시 원래대로 안돌리면 계속 증가함.
	}

	WinCheck(_hWnd);
	UpdateObjectFeature(_hWnd);

	m_BackgroundImage->Update(_hWnd);

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Update(_hWnd);
	}

	m_Player->Update(_hWnd, Objects);

	if (Stage02_isExit)
	{
		if (Stage02_babaIsWin == true)
		{
			m_FadeOut = true;
			m_NextGameState = STAGE_SELECT;
		}
		else if (Stage02_babaIsDead == true)
		{
			Init(m_hInstance);
		}
	}

	FadeUpdate();
}

void SceneStage02::SpriteUpdate(HWND _hWnd)
{
	for (auto& v : tiles)
	{
		v->Update(_hWnd);
	}

	for (auto& v2 : grass_tiles)
	{
		v2->Update(_hWnd);
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SpriteUpdate(_hWnd);
	}

	m_Player->SpriteUpdate(_hWnd);
}

void SceneStage02::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		break;
	case WM_MOUSEMOVE:
		break;
	}
}

void SceneStage02::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			m_FadeOut = true;
			m_NextGameState = SETTING;
		}
		break;
		case VK_RETURN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
		}
		break;
		case VK_UP:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_DOWN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y + BLOCK_SIZE > (WINDOW_SIZE_Y - 39))
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_LEFT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_RIGHT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x + BLOCK_SIZE > WINDOW_SIZE_X)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		}
		break;
	}
}

void SceneStage02::StopAll()
{
	FadeStopAll();

	if (*m_CurrentGameState != SETTING)
		m_Sound->StopSound();
}

void SceneStage02::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneStage02::GetStageState(bool * state, int size)
{
	for (int idx = 0; idx < size; idx++)
	{
		m_StageState[idx] = state[idx];
	}
}

void SceneStage02::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

const bool SceneStage02::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneStage02::GetGridMode(bool * value)
{
	m_CurrentGridMode = value;
}

void SceneStage02::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneStage02::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneStage02::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{
	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneStage02::FadeIn()
{
	m_alpha -= ALPHA_SPEED;
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneStage02::FadeOut()
{
	m_alpha += ALPHA_SPEED;
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}

void SceneStage02::FadeStopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;
}

void SceneStage02::UpdateObjectFeature(HWND hWnd)
{
	if (Objects.size() != 0)
	{
		for (auto& v : Objects)
		{
			///////////////////////////////////////////////////////////////////////////////// YOU 기준
			GameObject* target = &Objects[2];

			POINT targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			GameObject* targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			GameObject* targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_BABA) // BABA IS YOU
				{
					m_Player->SetLife(false);
					m_Player->SetCanMove(false);
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS YOU
				{
					if (m_Player->GetMoveTimer() == 0)
					{
						m_Player->SetLife(false);
						m_Player->SetCanMove(false);
					}
					/*m_Player->SetLife(true);
					m_Player->SetCanMove(true);
					m_Timer = 0;

					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::PUSH);
							v2.SetPawn(true);
						}
					}

					m_Player->SetState(OBJECTSTATE::STOPPED);*/
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_WALL) // WALL IS YOU
				{
					m_Player->SetLife(true);
					m_Player->SetCanMove(true);
					m_Timer = 0;
				}
			}
			
			if (m_Player->GetMoveTimer() == 0)
			{
				if ((targetObj1 != nullptr && targetObj2 == nullptr) || (targetObj1 == nullptr && targetObj2 != nullptr) || (targetObj1 == nullptr && targetObj2 == nullptr)) // 바바 사망
				{
					m_Player->SetLife(false);
					m_Player->SetCanMove(false);
				}
			}

			BabaIsDead(hWnd);


			///////////////////////////////////////////////////////////////////////////////// 깃발
			target = &Objects[0];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_WIN) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::FLAG_IS_WIN;
				}
			}


			///////////////////////////////////////////////////////////////////////////////// STOP 기준
			target = &Objects[8];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS STOP
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::STOP);
						}
					}
				}
			}
			else if ((targetObj1 != nullptr && targetObj2 == nullptr) || (targetObj1 == nullptr && targetObj2 != nullptr) || (targetObj1 == nullptr && targetObj2 == nullptr))
			{
				for (auto& v2 : Objects)
				{
					if (v2.GetKind() == FLAG)
					{
						v2.SetProperty(PROPERTY::OVERRIDE);
					}
				}
			}


			///////////////////////////////////////////////////////////////////////////////// WIN 기준
			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::FLAG_IS_WIN;
				}
			}


			///////////////////////////////////////////////////////////////////////////////// WIN 기준
			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_FLAG) // FLAG IS WIN
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == FLAG)
						{
							v2.SetProperty(PROPERTY::OVERRIDE);
						}
					}
					m_WinCondition = WINCONDITION::FLAG_IS_WIN;
				}
			}


			target = &Objects[5];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);
			GameObject* targetObj3 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			GameObject* targetObj4 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if ((targetObj1 == nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
			else if ((targetObj1 != nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
			else if ((targetObj1 == nullptr && targetObj2 != nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_WinCondition = WINCONDITION::CAN_NOT_WIN;
				}
			}
		}
	}
}

void SceneStage02::WinCheck(HWND hWnd)
{
	switch (m_WinCondition)
	{
	case FLAG_IS_WIN:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == FLAG)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						BabaIsWin(hWnd);
						m_PuzzleCompleted = true;
					}
				}
			}
		}
	}
	break;
	}
}

void SceneStage02::BabaIsDead(HWND hWnd)
{
	if (m_Timer > 60)
	{
		if (Stage02_isFirst)
		{
			Stage02_babaIsDead = true;
			Stage02_babaIsWin = false;
			Stage02_isFirst = false;
			m_DefeatSound->PlaySound(false, 1.0f);
			DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
		}

		m_Timer = 0;
	}
}

void SceneStage02::BabaIsWin(HWND hWnd)
{
	m_Player->SetLife(true);
	m_Player->SetCanMove(false);

	if (Stage02_isFirst)
	{
		Stage02_babaIsDead = false;
		Stage02_babaIsWin = true;
		Stage02_isFirst = false;
		m_Sound->StopSound();
		m_CompleteSound->PlaySound(false, 1.0f);
		DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
	}
}

const bool SceneStage02::GetPuzzleCompleted() const
{
	return m_PuzzleCompleted;
}

BOOL CALLBACK SceneStage02::DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hDialogDC, hDialogMemDC, hDialogBitDC;
	static RECT DialogCRT; // 직사각형모양 범 위 선언
	PAINTSTRUCT DialogPS;
	static HBITMAP hDialogBitmap, hDialogOldBitmap;

	static HBITMAP hButtonBitmap;

	static HWND _hWnd;

	static ObjectImage* DialogBackground;
	static 	ObjectImage* DialogNotifyDefeatImage;
	static 	ObjectImage* DialogNotifyConguratulationImage;
	static 	ObjectImage* DialogSpaceImage;
	static 	ObjectImage* DialogEnterImage;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		_hWnd = (HWND)GetWindowLongPtrA(hWnd, GWLP_HWNDPARENT);
		GetClientRect(hWnd, &DialogCRT);

		DialogBackground = new ObjectImage();
		DialogBackground->LoadSpriteImage(L"Data/Images/Title/TitleBackground.png", 792, 432, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2), DialogCRT.right, DialogCRT.bottom);

		DialogNotifyDefeatImage = new ObjectImage();
		DialogNotifyDefeatImage->LoadSpriteImage(L"Data/Images/Notify_Defeat.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogNotifyConguratulationImage = new ObjectImage();
		DialogNotifyConguratulationImage->LoadSpriteImage(L"Data/Images/Notify_Congratulation.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogSpaceImage = new ObjectImage();
		DialogSpaceImage->LoadSpriteImage(L"Data/Images/Space.png", 103, 50, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 25, 70, 50);

		DialogEnterImage = new ObjectImage();
		DialogEnterImage->LoadSpriteImage(L"Data/Images/Map/Enter.png", 48, 24, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 75, 70, 50);

		SetTimer(hWnd, 3, 1, NULL);
		SetTimer(hWnd, 4, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 3:
		{
			if (GetAsyncKeyState(VK_RETURN) & 0x8000)
			{
				Stage02_isExit = true;

				delete DialogBackground;
				delete DialogNotifyDefeatImage;
				delete DialogNotifyConguratulationImage;
				delete DialogSpaceImage;
				delete DialogEnterImage;

				EndDialog(hWnd, 0);
			}
		}
		break;
		case 4:
		{
			if (DialogBackground)
				DialogBackground->Update(hWnd);

			if (DialogNotifyDefeatImage)
				DialogNotifyDefeatImage->Update(hWnd);

			if (DialogNotifyConguratulationImage)
				DialogNotifyConguratulationImage->Update(hWnd);

			if (DialogSpaceImage)
				DialogSpaceImage->Update(hWnd);

			if (DialogEnterImage)
				DialogEnterImage->Update(hWnd);
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hDialogDC = BeginPaint(hWnd, &DialogPS);

		hDialogMemDC = CreateCompatibleDC(hDialogDC);
		hDialogBitmap = CreateCompatibleBitmap(hDialogDC, DialogCRT.right, DialogCRT.bottom);
		hDialogOldBitmap = (HBITMAP)SelectObject(hDialogMemDC, hDialogBitmap);
		FillRect(hDialogMemDC, &DialogCRT, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		if (DialogBackground)
			DialogBackground->Render(hDialogMemDC, hWnd);

		// 패배일 시
		if (DialogNotifyDefeatImage && Stage02_babaIsDead)
			DialogNotifyDefeatImage->Render(hDialogMemDC, hWnd);

		// 성공일 시
		if (DialogNotifyConguratulationImage && Stage02_babaIsWin)
			DialogNotifyConguratulationImage->Render(hDialogMemDC, hWnd);

		if (DialogSpaceImage)
			DialogSpaceImage->Render(hDialogMemDC, hWnd);

		if (DialogEnterImage)
			DialogEnterImage->Render(hDialogMemDC, hWnd);

		BitBlt(hDialogDC, 0, 0, DialogCRT.right, DialogCRT.bottom, hDialogMemDC, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(hDialogMemDC, hDialogOldBitmap));
		DeleteDC(hDialogMemDC);

		EndPaint(hWnd, &DialogPS);
	}
	break;
	}
	return 0;
}