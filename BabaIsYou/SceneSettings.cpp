#include "stdafx.h"
#include "SceneSettings.h"

SceneSettings::SceneSettings()
{
	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Setting/SettingBackground.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	m_GridTextImage = new ObjectImage();
	m_GridTextImage->LoadSpriteImage(L"Data/Images/Setting/Grid.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 25, 860, 50);
	m_GridTextHoveredImage = new ObjectImage();
	m_GridTextHoveredImage->LoadSpriteImage(L"Data/Images/Setting/GridHovered.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 25, 860, 50);
	m_GridTextSelectedImage = new ObjectImage();
	m_GridTextSelectedImage->LoadSpriteImage(L"Data/Images/Setting/GridSelected.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 25, 860, 50);
	m_GridTextSelectedHoveredImage = new ObjectImage();
	m_GridTextSelectedHoveredImage->LoadSpriteImage(L"Data/Images/Setting/GridSelectedHovered.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 25, 860, 50);

	m_ReturnMenuTextImage = new ObjectImage();
	m_ReturnMenuTextImage->LoadSpriteImage(L"Data/Images/Setting/ReturnMenu.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 85, 860, 50);
	m_ReturnMenuHoveredImage = new ObjectImage();
	m_ReturnMenuHoveredImage->LoadSpriteImage(L"Data/Images/Setting/ReturnMenuHovered.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 85, 860, 50);

	m_ReturnTextImage = new ObjectImage();
	m_ReturnTextImage->LoadSpriteImage(L"Data/Images/Setting/Return.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 145, 860, 50);
	m_ReturnHoveredImage = new ObjectImage();
	m_ReturnHoveredImage->LoadSpriteImage(L"Data/Images/Setting/ReturnHovered.png", 858, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 145, 860, 50);

	m_CursorImage = new ObjectImage();
	m_CursorImage->LoadSpriteImage(L"Data/Images/Cursor.png", 24, 24, 3, (WINDOW_SIZE_X / 2) - 475, (WINDOW_SIZE_Y / 4) + 25, 50, 50);

	m_GridButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 25);
	m_GridButton.SetSize(860, 50);
	m_ReturnMenuButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 85);
	m_ReturnMenuButton.SetSize(860, 50);
	m_ReturnButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4) + 145);
	m_ReturnButton.SetSize(860, 50);

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Setting/baba.ogg");

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_CanEnterScene = true;
	m_SelectNumber = 0;

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);
}

SceneSettings::~SceneSettings()
{
	delete m_BackgroundImage;

	delete m_GridTextImage;
	delete m_GridTextHoveredImage;
	delete m_GridTextSelectedImage;
	delete m_GridTextSelectedHoveredImage;

	delete m_ReturnMenuTextImage;
	delete m_ReturnMenuHoveredImage;

	delete m_ReturnTextImage;
	delete m_ReturnHoveredImage;

	delete m_CursorImage;
	delete m_Sound;
	delete m_MoveSound;

	delete m_FadeImage;
}

void SceneSettings::Init()
{
	m_Sound->PlaySound(true, 0.3f);

	FadeInit();
}

void SceneSettings::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);

	if ((m_GridButton.IsMouseCollision(mouse) == false) && (*m_CurrentGridMode == false))
	{
		m_GridTextImage->Render(_hdc, _hWnd);
	}
	else if ((m_GridButton.IsMouseCollision(mouse) == true) && (*m_CurrentGridMode == false))
	{
		m_GridTextHoveredImage->Render(_hdc, _hWnd);
	}
	else if ((m_GridButton.IsMouseCollision(mouse) == false) && (*m_CurrentGridMode == true))
	{
		m_GridTextSelectedImage->Render(_hdc, _hWnd);
	}
	else if((m_GridButton.IsMouseCollision(mouse) == true) && (*m_CurrentGridMode == true))
	{
		m_GridTextSelectedHoveredImage->Render(_hdc, _hWnd);
	}

	if (m_ReturnMenuButton.IsMouseCollision(mouse))
		m_ReturnMenuHoveredImage->Render(_hdc, _hWnd);
	else
		m_ReturnMenuTextImage->Render(_hdc, _hWnd);

	if (m_ReturnButton.IsMouseCollision(mouse))
		m_ReturnHoveredImage->Render(_hdc, _hWnd);
	else
		m_ReturnTextImage->Render(_hdc, _hWnd);

	m_CursorImage->Render(_hdc, _hWnd);
	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneSettings::Update(HWND _hWnd)
{
	switch (m_SelectNumber)
	{
	case 0:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 475, (WINDOW_SIZE_Y / 4) + 25);
		break;
	case 1:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 475, (WINDOW_SIZE_Y / 4) + 85);
		break;
	case 2:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 475, (WINDOW_SIZE_Y / 4) + 145);
		break;
	}

	FadeUpdate();
}

void SceneSettings::SpriteUpdate(HWND _hWnd)
{
	m_BackgroundImage->Update(_hWnd);
	m_CursorImage->Update(_hWnd);

	m_GridTextImage->Update(_hWnd);
	m_GridTextHoveredImage->Update(_hWnd);
	m_GridTextSelectedImage->Update(_hWnd);
	m_GridTextSelectedHoveredImage->Update(_hWnd);

	m_ReturnMenuTextImage->Update(_hWnd);
	m_ReturnMenuHoveredImage->Update(_hWnd);

	m_ReturnTextImage->Update(_hWnd);
	m_ReturnHoveredImage->Update(_hWnd);
}

void SceneSettings::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_CHAR:
	{
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
			if (m_SelectNumber > 0)
			{
				m_SelectNumber--;
			}
			else
			{
				m_SelectNumber = 2;
			}

			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_DOWN:
		{
			if (m_SelectNumber < 2)
			{
				m_SelectNumber++;
			}
			else
			{
				m_SelectNumber = 0;
			}

			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_ESCAPE:
		{
			m_FadeOut = true;
			m_NextGameState = m_PreviousGameState;
		}
		break;
		case VK_RETURN:
		{
			switch (m_SelectNumber)
			{
			case 0:
			{
				if (*m_CurrentGridMode)
					*m_CurrentGridMode = false;
				else
					*m_CurrentGridMode = true;
			}
			break;
			case 1:
			{
				m_FadeOut = true;
				m_NextGameState = TITLE;
			}
			break;
			case 2:
			{
				m_FadeOut = true;
				m_NextGameState = m_PreviousGameState;
			}
			break;
			}
		}
		break;
		}
		break;
	}
	break;
	}
}

void SceneSettings::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_LBUTTONDOWN:
	{
		::GetCursorPos(&mouse);
		ScreenToClient(hWnd, &mouse);

		if (m_GridButton.IsMouseCollision(mouse))
		{
			if (*m_CurrentGridMode)
				*m_CurrentGridMode = false;
			else
				*m_CurrentGridMode = true;
		}

		if (m_ReturnMenuButton.IsMouseCollision(mouse))
		{
			m_FadeOut = true;
			m_NextGameState = TITLE;
		}

		if (m_ReturnButton.IsMouseCollision(mouse))
		{
			m_FadeOut = true;
			m_NextGameState = m_PreviousGameState;
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
		::GetCursorPos(&mouse);
		ScreenToClient(hWnd, &mouse);
	}
	break;
	}
}

void SceneSettings::StopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;

	m_Sound->StopSound();
}

void SceneSettings::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneSettings::GetGridMode(bool * value)
{
	m_CurrentGridMode = value;
}

void SceneSettings::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

void SceneSettings::SetPreviousGameState(int state)
{
	m_PreviousGameState = state;
}

const bool SceneSettings::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneSettings::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneSettings::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneSettings::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{
	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneSettings::FadeIn()
{
	m_alpha = 0 - 1;
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneSettings::FadeOut()
{
	m_alpha = 255 + 1;
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}