#include "stdafx.h"
#include "Sound.h"

#define MAX_SOUND 100

/*int Sound::CreateSound(char * filePath)
{
	int index = 0;

	for (int i = 1; i < MAX_SOUND; i++)
	{
		if (m_SoundList.find(i) == m_SoundList.end())
		{
			index = i;
			break;
		}
	}

	ISoundSource* tempSound = m_Engine->addSoundSourceFromFile(filePath);
	if (tempSound == NULL)
	{
		cout << "사운드 생성 실패" << endl;
		return -1;
	}

	m_SoundList.insert(pair<int, ISoundSource*>(index, tempSound));

	return index;
}*/

/*void Sound::DeleteSound(int index)
{
	map<int, ISoundSource*>::iterator iter;

	iter = m_SoundList.find(index);

	if (iter == m_SoundList.end())
	{
		cout << "해당 인덱스에 해당하는 파일이 없음 →" << index << endl;
		return;
	}

	(*iter).second->drop();
	m_SoundList.erase(index);
}*/

// 인덱스, 계속반복?, 소리크기(0~1)
/*void Sound::PlaySound(int index, bool isLoop, float volume)
{
	map<int, ISoundSource*>::iterator iter;

	iter = m_SoundList.find(index);

	if (iter == m_SoundList.end())
	{
		cout << "해당 인덱스에 해당하는 파일이 없음 →" << index << endl;
		return;
	}

	(*iter).second->setDefaultVolume(volume);
	m_Engine->play2D((*iter).second, isLoop);
}*/

void Sound::CreateSound(const char * filePath)
{
	m_SoundSource = m_Engine->addSoundSourceFromFile(filePath);
	if (m_SoundSource == NULL)
	{
		cout << "사운드 생성 실패" << endl;
		return;
	}
}

void Sound::PlaySound(bool isLoop, float volume)
{
	m_SoundSource->setDefaultVolume(volume);
	if(!(m_Engine->isCurrentlyPlaying(m_SoundSource)))
		m_Engine->play2D(m_SoundSource->getName(), isLoop);
}

void Sound::StopSound()
{
	m_Engine->stopAllSounds();
	cout << "사운드가 중지됨" << endl;
}

Sound::Sound()
{
	m_Engine = createIrrKlangDevice();

	if (!m_Engine)
	{
		cout << "사운드 초기화 실패" << endl;
	}
}

Sound::~Sound()
{
	/*map<int, ISoundSource*>::iterator iter;

	for (iter = m_SoundList.begin(); iter != m_SoundList.end(); iter++)
	{
		DeleteSound((*iter).first);
	}

	m_Engine->drop();
	m_SoundList.clear();*/

	m_Engine->removeSoundSource(m_SoundSource);
	m_Engine->drop();
}
