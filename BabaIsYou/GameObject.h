#pragma once

class ObjectImage;
class Sound;
class Baba;

class GameObject
{
private:
	ObjectImage* m_Image;

	ObjectImage* m_WallImage;
	ObjectImage* m_RockImage;
	ObjectImage* m_FlagImage;

	POINT m_Position;
	POINT m_Size;
	
	bool m_Visible;
	int m_Kind;

	bool m_IsPawn = false; // 캐릭터가 빙의될 수 있는가?
	OBJECTSTATE m_MyState = OBJECTSTATE::STOPPED;

	POINT m_GoalPosition;
	int m_MoveTimer = 0;
	int m_WalkingDistance = BLOCK_SIZE;
	int m_Velocity = 5;
	OBJECTDIRECTION m_Direction = OBJECTDIRECTION::NONE;

	int m_Property;
	bool m_VariationIsFinished = true;


public:
	GameObject();
	GameObject(int positionX, int positionY, int sizeX, int sizeY);
	GameObject(const GameObject& rhs);
	~GameObject();

public:
	void Init();
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void Render(HDC _hdc, HWND _hWnd);

	void SetImage(LPCTSTR fileName, int spriteGaroPixel, int spriteSeroPixel, int spriteEntireCount);

	void SetPosition(int positionX, int positionY);
	void SetGoalPosition(int goalX, int goalY);
	const POINT GetPosition() const;

	void SetSize(int sizeX, int sizeY);
	void SetSize(int size);
	const POINT GetSize() const;

	void SetVisible(bool value);
	const bool GetVisible() const;

	void SetKind(int value);
	const int GetKind() const;

	void SetPawn(bool value);
	const bool GetPawn() const;

	void SetProperty(int value);
	const int GetProperty() const;

	int CollisionCheck(vector<GameObject>& container);
	bool IsMouseCollision(POINT& mouseCoordinate);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void SetState(OBJECTSTATE state);
	const OBJECTSTATE GetState() const;

	void SetDirection(OBJECTDIRECTION direction);
	const OBJECTDIRECTION GetDirection() const;

	void SetMoveTimer(int value);
	void SetVariationIsFinished(bool value);
	const bool GetVariationIsFinished() const;

	bool IsPointInRect(POINT pt, POINT obj_pt);
	GameObject* GetObjects(POINT coord, vector<GameObject>& container);
	bool FindNearObjects(Baba* p, GameObject* rhs, vector<GameObject>& container, OBJECTSTATE obj_state, OBJECTDIRECTION obj_direction);
};

