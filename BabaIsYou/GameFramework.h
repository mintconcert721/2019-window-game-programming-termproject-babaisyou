#pragma once

class SceneTitle;
class SceneSelect;
class SceneTutorial;
class SceneStage01;
class SceneStage02;
class SceneStage03;
class SceneStage04;
class SceneSettings;
class ObjectImage;
class Sound;

class GameFramework
{
private:
	HINSTANCE m_hInstance;
	HWND m_hWnd;

	int m_GameState = TITLE; // TITLE;
	int m_PrevGameState;
	bool m_GridState = false;

	SceneTitle* m_SceneTitle;
	SceneSelect* m_SceneSelect;
	SceneTutorial* m_SceneTutorial;
	SceneSettings* m_SceneSettings;
	SceneStage01* m_SceneStage01;
	SceneStage02* m_SceneStage02;
	SceneStage03* m_SceneStage03;
	SceneStage04* m_SceneStage04;

	bool m_StageState[7] = { false, false, true, false, false, false, false };

public:
	void Init(HINSTANCE _hInstance, HWND _hWnd);
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void SetGameState(int state);
	const int GetCurrentState();

public:
	GameFramework();
	~GameFramework();
};

