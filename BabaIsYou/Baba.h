#pragma once
#include <set>

class ObjectImage;
class Sound;
class GameObject;

#define IMAGE_MAX_NUMBER 4

class Baba
{
private:
	ObjectImage* m_UpImage[5];
	ObjectImage* m_DownImage[5];
	ObjectImage* m_LeftImage[5];
	ObjectImage* m_RightImage[5];

	ObjectImage* m_WallImage;
	ObjectImage* m_RockImage;

	int m_ImageIndex = 0;

	POINT m_Position;
	POINT m_Size;

	bool m_IsLife = true;
	bool m_CanMove = true;

	// �̵� ��
	POINT m_GoalPosition;
	int m_MoveTimer = 0;
	int m_WalkingDistance = BLOCK_SIZE;
	int m_Velocity = 5;
	bool m_MyState = OBJECTSTATE::STOPPED;
	OBJECTDIRECTION m_Direction = OBJECTDIRECTION::RIGHT;
	
	int m_Kind;
	int m_Property;

public:
	bool m_VariationFinished = true;
	set<GameObject*> tempContainer;

public:
	Baba();
	~Baba();

public:
	void Init();
	void Update(HWND _hWnd, vector<GameObject>& container);
	void SpriteUpdate(HWND _hWnd);
	void Render(HDC _hdc, HWND _hWnd);

	void SetPosition(int positionX, int positionY);
	void SetGoalPosition(int goalX, int goalY);
	const POINT GetPosition() const;

	void SetSize(int sizeX, int sizeY);
	void SetSize(int size);
	const POINT GetSize() const;

	void SetKind(int value);
	const int GetKind() const;

	void SetState(bool obj_state);
	const bool GetState() const;

	void SetDirection(OBJECTDIRECTION direction);
	const OBJECTDIRECTION GetDirection() const;

	void SetMoveTimer(int value);
	const int GetMoveTimer() const;

	void SetLife(bool value);
	const bool IsLife() const;

	void SetCanMove(bool value);
	const bool IsCanMove() const;

	bool IsPointInRect(POINT pt, POINT obj_pt);
	GameObject* GetObjects(POINT coord, vector<GameObject>& container);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	bool CheckObjects(vector<GameObject>& container);
};

