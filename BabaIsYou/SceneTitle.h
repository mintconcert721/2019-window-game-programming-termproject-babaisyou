#pragma once

class Sound;
class ObjectImage;

class SceneTitle
{
private:
	ObjectImage* m_BackgroundImage;
	ObjectImage* m_TitleNameImage;

	ObjectImage* m_StartTextImage;
	ObjectImage* m_SettingsTextImage;
	ObjectImage* m_ExitTextImage;

	ObjectImage* m_StartTextHoveredImage;
	ObjectImage* m_SettingsTextHoveredImage;
	ObjectImage* m_ExitTextHoveredImage;

	ObjectImage* m_CursorImage;

	GameObject m_StartButton;
	GameObject m_SettingButton;
	GameObject m_ExitButton;

	Sound* m_Sound;
	Sound* m_MoveSound;
	POINT mouse;

	int* m_CurrentGameState;
	bool m_CanEnterScene;
	int m_SelectNumber;

	ObjectImage* m_FadeImage;

	bool m_FadeOut = false;
	bool m_FadeIn = true;
	bool m_OngoingFadeOutEffectFinished = false;
	bool m_OngoingFadeInEffectFinished = false;
	int m_alpha = 255;

	int m_NextGameState;

public:
	SceneTitle();
	~SceneTitle();

public:
	void Init();
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void StopAll();
	void GetGameState(int* state);
	void SetCanEnter(bool value);
	const bool GetCanEnter() const;

	void FadeInit();
	void FadeUpdate();
	void FadeRender(HDC _hdc, HWND _hWnd, int _alpha);
	void FadeIn();
	void FadeOut();
};

