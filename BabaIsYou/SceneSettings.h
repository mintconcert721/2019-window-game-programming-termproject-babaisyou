#pragma once

class Sound;
class ObjectImage;

class SceneSettings
{
private:
	ObjectImage* m_BackgroundImage;

	ObjectImage* m_GridTextImage;
	ObjectImage* m_GridTextHoveredImage;
	ObjectImage* m_GridTextSelectedImage;
	ObjectImage* m_GridTextSelectedHoveredImage;

	ObjectImage* m_ReturnMenuTextImage;
	ObjectImage* m_ReturnMenuHoveredImage;

	ObjectImage* m_ReturnTextImage;
	ObjectImage* m_ReturnHoveredImage;

	ObjectImage* m_CursorImage;

	ObjectImage* m_FadeImage;

	GameObject m_GridButton;
	GameObject m_ReturnMenuButton;
	GameObject m_ReturnButton;

	Sound* m_Sound;
	Sound* m_MoveSound;
	POINT mouse;

	int m_PreviousGameState;
	int* m_CurrentGameState;
	bool* m_CurrentGridMode;

	bool m_CanEnterScene;
	int m_SelectNumber;

	bool m_FadeIn = false;
	bool m_OngoingFadeInEffectFinished = false;
	int m_alpha = 255;
	bool m_FadeOut = false;
	bool m_OngoingFadeOutEffectFinished = false;

	int m_NextGameState;

public:
	SceneSettings();
	~SceneSettings();

public:
	void Init();
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void StopAll();
	void GetGameState(int* state);
	void GetGridMode(bool* value);
	void SetCanEnter(bool value);
	void SetPreviousGameState(int state);

	const bool GetCanEnter() const;

	void FadeInit();
	void FadeUpdate();
	void FadeRender(HDC _hdc, HWND _hWnd, int _alpha);
	void FadeIn();
	void FadeOut();
};

