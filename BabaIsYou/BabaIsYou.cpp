﻿// BabaIsYou.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "BabaIsYou.h"
#include "GameFramework.h"

#define MAX_LOADSTRING 100

#define BACKGROUND_TIMER 0

// 전역 변수:
HINSTANCE g_hInstance;
WCHAR szTitle[MAX_LOADSTRING]; // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING]; // 기본 창 클래스 이름입니다.

GameFramework* g_GameFramework; // 게임 프레임워크

// 윈도우 크기 변수:
int uWindowWidth = 0;
int uWindowHeight = 0;

int ScreenHalfX = 0;
int ScreenHalfY = 0;

// 이 코드 모듈에 포함된 함수의 선언을 전달합니다:
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 여기에 코드를 입력합니다.

    // 전역 문자열을 초기화합니다.
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_BABAISYOU, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 응용 프로그램 초기화를 수행합니다:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    MSG Message;

	g_GameFramework = new GameFramework();

    // 기본 메시지 루프입니다:
    while (GetMessage(&Message, nullptr, 0, 0))
    {
		TranslateMessage(&Message);
		DispatchMessage(&Message);
    }

	if (g_GameFramework)
		delete g_GameFramework;

    return (int)Message.wParam;
}



//
//  함수: MyRegisterClass()
//
//  용도: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BABAISYOU));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_BABAISYOU));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   용도: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   주석:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   g_hInstance = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   RECT rt = { 0, 0, WINDOW_SIZE_X, WINDOW_SIZE_Y };
   AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW | WS_SYSMENU | WS_CAPTION, false);

   uWindowWidth = rt.right - rt.left;
   uWindowHeight = rt.bottom - rt.top;

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      152, 70, uWindowWidth, uWindowHeight, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   RECT rectView;
   GetClientRect(hWnd, &rectView);
   rectView.right = 2 * uWindowWidth - rectView.right;
   rectView.bottom = 2 * uWindowHeight - rectView.bottom;

   SetWindowPos(hWnd, NULL, 0, 0, rectView.right, rectView.bottom, SWP_NOZORDER | SWP_NOMOVE);

   if(g_GameFramework)
	   g_GameFramework->Init(g_hInstance, hWnd);

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  용도: 주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;
	static HBITMAP hBitmap, hOldBitmap;

	static POINT mouse;

    switch (message)
    {
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);
		ScreenHalfX = (crt.right - crt.left) / 2;
		ScreenHalfY = (crt.bottom - crt.top) / 2;

		SetTimer(hWnd, 1, 1, NULL);
		SetTimer(hWnd, 2, 150, NULL);
	}
	break;
	case WM_GETMINMAXINFO:
	{
		((MINMAXINFO*)lParam)->ptMaxTrackSize.x = WINDOW_SIZE_X;
		((MINMAXINFO*)lParam)->ptMaxTrackSize.y = WINDOW_SIZE_Y;
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = WINDOW_SIZE_X;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = WINDOW_SIZE_Y;
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 1:
		{
			if (g_GameFramework)
				g_GameFramework->Update(hWnd);
		}
		break;
		case 2:
		{
			if (g_GameFramework)
				g_GameFramework->SpriteUpdate(hWnd);
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
	{
		if (g_GameFramework)
			g_GameFramework->MouseMessage(hWnd, message, wParam, lParam);
	}
	break;
	case WM_CHAR:
	case WM_KEYDOWN:
	{
		if (g_GameFramework)
			g_GameFramework->KeyboardMessage(hWnd, message, wParam, lParam);
	}
	break;
    case WM_COMMAND:
	{
	}
	break;
    case WM_PAINT:
    {
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &crt, (HBRUSH)GetStockObject(BLACK_BRUSH)); //도화지 색 변경

		if(g_GameFramework)
			g_GameFramework->Render(memdc, hWnd);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
    }
       break;
    case WM_DESTROY:
	{
		PostQuitMessage(0);
	}
	break;
    default:
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	break;
    }

    return 0;
}
