#pragma once

class Sound;
class ObjectImage;
class GameObject;

class SceneSelect
{
private:
	ObjectImage* m_BackgroundImage;

	ObjectImage* m_StageNameBackgroundImage1;
	ObjectImage* m_StageNameBackgroundImage2;
	ObjectImage* m_StageNameBackgroundImage3;
	ObjectImage* m_StageNameBackgroundImage4;
	ObjectImage* m_StageNameBackgroundImage5;
	ObjectImage* m_StageNameBackgroundImage6;
	ObjectImage* m_StageNameBackgroundImage7;
	ObjectImage* m_StageNameBackgroundImage8;
	ObjectImage* m_StageNameBackgroundImage9;
	ObjectImage* m_StageNameBackgroundImage10;
	ObjectImage* m_StageNameBackgroundImage11;
	ObjectImage* m_StageNameBackgroundImage12;
	ObjectImage* m_StageNameBackgroundImage13;
	ObjectImage* m_StageNameBackgroundImage14;

	ObjectImage* m_DotImage1;
	ObjectImage* m_DotImage2;
	ObjectImage* m_DotImage3;
	ObjectImage* m_DotImage4;
	ObjectImage* m_DotImage5;
	ObjectImage* m_DotImage6;
	ObjectImage* m_DotImage7;
	ObjectImage* m_DotImage8;
	ObjectImage* m_DotImage9;

	ObjectImage* m_Stage00NameImage;
	ObjectImage* m_Stage01NameImage;
	ObjectImage* m_Stage02NameImage;
	ObjectImage* m_Stage03NameImage;
	ObjectImage* m_Stage04NameImage;
	ObjectImage* m_Stage05NameImage;

	ObjectImage* m_LineImage1;
	ObjectImage* m_LineImage2;

	ObjectImage* m_SpaceImage;
	ObjectImage* m_EnterTextImage;
	ObjectImage* m_KeyboardImage;
	ObjectImage* m_MoveTextImage;
	ObjectImage* m_FadeImage;

	Sound* m_Sound;
	Sound* m_MoveSound;
	Sound* m_IntroSound;

	int* m_CurrentGameState;
	bool* m_CurrentGridMode;
	bool m_StageState[7];

	bool m_CanEnterScene;

	GameObject GridArray[GRID_GARO][GRID_SERO];
	vector<GameObject> Objects;

	HBRUSH hBrush;
	HBRUSH hOldBrush;
	HPEN hPen;
	HPEN hOldPen;

	HINSTANCE m_hInstance;

	bool m_FadeIn = false;
	bool m_OngoingFadeInEffectFinished = false;
	int m_alpha = 255;
	bool m_FadeOut = false;
	bool m_OngoingFadeOutEffectFinished = false;

	int m_NextGameState;

public:
	SceneSelect();
	~SceneSelect();

public:
	void Init(HINSTANCE _hInstance);
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void StopAll();

	void GetGameState(int* state);
	void GetStageState(bool* state, int size);
	void SetCanEnter(bool value);
	const bool GetCanEnter() const;
	void GetGridMode(bool* value);

	void FadeInit();
	void FadeUpdate();
	void FadeRender(HDC _hdc, HWND _hWnd, int _alpha);
	void FadeIn();
	void FadeOut();

	static BOOL CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
};