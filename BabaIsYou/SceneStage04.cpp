#include "stdafx.h"
#include "resource.h"
#include "SceneStage04.h"
#include "Baba.h"

SceneStage04::SceneStage04()
{
	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			GridArray[garo][sero].SetPosition(garo * BLOCK_SIZE + (BLOCK_SIZE / 2), sero * BLOCK_SIZE + (BLOCK_SIZE / 2));
			GridArray[garo][sero].SetSize(BLOCK_SIZE);
			GridArray[garo][sero].SetKind(-1);
		}
	}

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Map/BG5.ogg");

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_CompleteSound = new Sound();
	m_CompleteSound->CreateSound("Data/Sounds/complete.ogg");

	m_DefeatSound = new Sound();
	m_DefeatSound->CreateSound("Data/Sounds/defeat.ogg");

	m_Player = new Baba();
	m_Player->SetKind(OBJECT_KIND::WALL);
	int px = 5, py = 16;
	m_Player->SetPosition(GridArray[px][py].GetPosition().x, GridArray[px][py].GetPosition().y);

	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Tutorial/TutorialBackground.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);

	Objects.resize(67);
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SetKind(-1);
		Objects[idx].SetVisible(false);
	}

	int a, b;

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 0, b = 1;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetImage(L"Data/Images/Map/text_baba.png", 24, 24, 3);
	Objects[0].SetProperty(PROPERTY::STOP);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 1, b = 1;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[1].SetProperty(PROPERTY::STOP);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 2, b = 1;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetImage(L"Data/Images/Map/text_you.png", 24, 24, 3);
	Objects[2].SetProperty(PROPERTY::STOP);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 0, b = 0;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetImage(L"Data/Images/Map/text_flag.png", 24, 24, 3);
	Objects[3].SetProperty(PROPERTY::STOP);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 1, b = 0;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[4].SetProperty(PROPERTY::STOP);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 2, b = 0;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetImage(L"Data/Images/Map/text_win.png", 24, 24, 3);
	Objects[5].SetProperty(PROPERTY::STOP);

	Objects[6].SetKind(TEXT_KIND::TEXT_ROCK);
	a = 5, b = 8;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetImage(L"Data/Images/Tutorial/text_rock.png", 24, 24, 3);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 6, b = 8;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_PUSH);
	a = 7, b = 8;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetImage(L"Data/Images/Tutorial/text_push.png", 24, 24, 3);
	Objects[8].SetProperty(PROPERTY::PUSH);

	Objects[9].SetKind(TEXT_KIND::TEXT_SKULL);
	a = 20, b = 8;
	Objects[9].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[9].SetSize(BLOCK_SIZE);
	Objects[9].SetVisible(true);
	Objects[9].SetImage(L"Data/Images/Stage04/text_skull.png", 24, 24, 3);
	Objects[9].SetProperty(PROPERTY::PUSH);

	Objects[10].SetKind(TEXT_KIND::TEXT_IS);
	a = 20, b = 9;
	Objects[10].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[10].SetSize(BLOCK_SIZE);
	Objects[10].SetVisible(true);
	Objects[10].SetImage(L"Data/Images/Map/text_is.png", 24, 24, 3);
	Objects[10].SetProperty(PROPERTY::PUSH);

	Objects[11].SetKind(TEXT_KIND::TEXT_DEFEAT);
	a = 20, b = 10;
	Objects[11].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[11].SetSize(BLOCK_SIZE);
	Objects[11].SetVisible(true);
	Objects[11].SetImage(L"Data/Images/Stage04/text_defeat.png", 24, 24, 3);
	Objects[11].SetProperty(PROPERTY::PUSH);

	Objects[12].SetKind(OBJECT_KIND::FLAG);
	a = 24, b = 12;
	Objects[12].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[12].SetSize(BLOCK_SIZE);
	Objects[12].SetVisible(true);
	Objects[12].SetImage(L"Data/Images/Tutorial/flag.png", 24, 24, 3);
	Objects[12].SetProperty(PROPERTY::OVERRIDE);

	for (auto idx = 13; idx < 16; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::ROCK);
		Objects[idx].SetPosition(GridArray[10][idx - 1].GetPosition().x, GridArray[10][idx - 1].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Tutorial/rock.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::PUSH);
	}

	for (auto idx = 16; idx < 26; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 16][13].GetPosition().x, GridArray[idx - 16][13].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 26; idx < 31; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[11][idx - 13].GetPosition().x, GridArray[11][idx - 13].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 31; idx < 41; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 13][6].GetPosition().x, GridArray[idx - 13][6].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 41; idx < 51; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 23][15].GetPosition().x, GridArray[idx - 23][15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 51; idx < 59; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[18][idx - 44].GetPosition().x, GridArray[18][idx - 44].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 59; idx < 67; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[27][idx - 52].GetPosition().x, GridArray[27][idx - 52].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetImage(L"Data/Images/Stage04/skull_front.png", 24, 24, 3);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}
}

SceneStage04::~SceneStage04()
{
	delete m_Player;

	delete m_BackgroundImage;
	delete m_FadeImage;

	delete m_Sound;
	delete m_MoveSound;
	delete m_CompleteSound;
	delete m_DefeatSound;
}

void SceneStage04::Init(HINSTANCE _hInstance)
{
	m_hInstance = _hInstance;

	m_Sound->PlaySound(true, 0.1f);

	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;

	m_Timer = 0;

	if (m_Player)
	{
		m_Player->SetLife(true);
		m_Player->SetCanMove(true);
		m_Player->SetPosition(GridArray[5][16].GetPosition().x, GridArray[5][16].GetPosition().y);
		m_Player->SetKind(OBJECT_KIND::BABA);
	}

	int a, b;

	Objects[0].SetKind(TEXT_KIND::TEXT_BABA);
	a = 0, b = 1;
	Objects[0].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[0].SetSize(BLOCK_SIZE);
	Objects[0].SetVisible(true);
	Objects[0].SetProperty(PROPERTY::STOP);

	Objects[1].SetKind(TEXT_KIND::TEXT_IS);
	a = 1, b = 1;
	Objects[1].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[1].SetSize(BLOCK_SIZE);
	Objects[1].SetVisible(true);
	Objects[1].SetProperty(PROPERTY::STOP);

	Objects[2].SetKind(TEXT_KIND::TEXT_YOU);
	a = 2, b = 1;
	Objects[2].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[2].SetSize(BLOCK_SIZE);
	Objects[2].SetVisible(true);
	Objects[2].SetProperty(PROPERTY::STOP);

	Objects[3].SetKind(TEXT_KIND::TEXT_FLAG);
	a = 0, b = 0;
	Objects[3].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[3].SetSize(BLOCK_SIZE);
	Objects[3].SetVisible(true);
	Objects[3].SetProperty(PROPERTY::STOP);

	Objects[4].SetKind(TEXT_KIND::TEXT_IS);
	a = 1, b = 0;
	Objects[4].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[4].SetSize(BLOCK_SIZE);
	Objects[4].SetVisible(true);
	Objects[4].SetProperty(PROPERTY::STOP);

	Objects[5].SetKind(TEXT_KIND::TEXT_WIN);
	a = 2, b = 0;
	Objects[5].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[5].SetSize(BLOCK_SIZE);
	Objects[5].SetVisible(true);
	Objects[5].SetProperty(PROPERTY::STOP);

	Objects[6].SetKind(TEXT_KIND::TEXT_ROCK);
	a = 5, b = 8;
	Objects[6].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[6].SetSize(BLOCK_SIZE);
	Objects[6].SetVisible(true);
	Objects[6].SetProperty(PROPERTY::PUSH);

	Objects[7].SetKind(TEXT_KIND::TEXT_IS);
	a = 6, b = 8;
	Objects[7].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[7].SetSize(BLOCK_SIZE);
	Objects[7].SetVisible(true);
	Objects[7].SetProperty(PROPERTY::PUSH);

	Objects[8].SetKind(TEXT_KIND::TEXT_PUSH);
	a = 7, b = 8;
	Objects[8].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[8].SetSize(BLOCK_SIZE);
	Objects[8].SetVisible(true);
	Objects[8].SetProperty(PROPERTY::PUSH);

	Objects[9].SetKind(TEXT_KIND::TEXT_SKULL);
	a = 20, b = 8;
	Objects[9].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[9].SetSize(BLOCK_SIZE);
	Objects[9].SetVisible(true);
	Objects[9].SetProperty(PROPERTY::PUSH);

	Objects[10].SetKind(TEXT_KIND::TEXT_IS);
	a = 20, b = 9;
	Objects[10].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[10].SetSize(BLOCK_SIZE);
	Objects[10].SetVisible(true);
	Objects[10].SetProperty(PROPERTY::PUSH);

	Objects[11].SetKind(TEXT_KIND::TEXT_DEFEAT);
	a = 20, b = 10;
	Objects[11].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[11].SetSize(BLOCK_SIZE);
	Objects[11].SetVisible(true);
	Objects[11].SetProperty(PROPERTY::PUSH);

	Objects[12].SetKind(OBJECT_KIND::FLAG);
	a = 24, b = 12;
	Objects[12].SetPosition(GridArray[a][b].GetPosition().x, GridArray[a][b].GetPosition().y);
	Objects[12].SetSize(BLOCK_SIZE);
	Objects[12].SetVisible(true);
	Objects[12].SetProperty(PROPERTY::OVERRIDE);

	for (auto idx = 13; idx < 16; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::ROCK);
		Objects[idx].SetPosition(GridArray[10][idx - 1].GetPosition().x, GridArray[10][idx - 1].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::PUSH);
	}

	for (auto idx = 16; idx < 26; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 16][13].GetPosition().x, GridArray[idx - 16][13].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 26; idx < 31; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[11][idx - 13].GetPosition().x, GridArray[11][idx - 13].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 31; idx < 41; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 13][6].GetPosition().x, GridArray[idx - 13][6].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 41; idx < 51; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[idx - 23][15].GetPosition().x, GridArray[idx - 23][15].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 51; idx < 59; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[18][idx - 44].GetPosition().x, GridArray[18][idx - 44].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	for (auto idx = 59; idx < 67; idx++)
	{
		Objects[idx].SetKind(OBJECT_KIND::SKULL);
		Objects[idx].SetPosition(GridArray[27][idx - 52].GetPosition().x, GridArray[27][idx - 52].GetPosition().y);
		Objects[idx].SetSize(BLOCK_SIZE);
		Objects[idx].SetVisible(true);
		Objects[idx].SetProperty(PROPERTY::OVERRIDE);
	}

	Stage04_isExit = false;
	Stage04_isFirst = true;
	Stage04_babaIsDead = false;
	Stage04_babaIsWin = false;
}

void SceneStage04::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);

	for (int sero = 0; sero < GRID_SERO; sero++) // 한 칸의 크기 : 50
	{
		for (int garo = 0; garo < GRID_GARO; garo++) // 한 칸의 크기 : 50
		{
			if (*m_CurrentGridMode)
			{
				hPen = CreatePen(PS_SOLID, 1, RGB(51, 51, 51));
				hOldPen = (HPEN)SelectObject(_hdc, hPen);
				hBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				hOldBrush = (HBRUSH)SelectObject(_hdc, hBrush);

				Rectangle(_hdc, GridArray[garo][sero].GetPosition().x - (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y - (GridArray[garo][sero].GetSize().y / 2),
					GridArray[garo][sero].GetPosition().x + (GridArray[garo][sero].GetSize().x / 2), GridArray[garo][sero].GetPosition().y + (GridArray[garo][sero].GetSize().y / 2));

				SelectObject(_hdc, hOldBrush);
				DeleteObject(hBrush);
				SelectObject(_hdc, hOldPen);
				DeleteObject(hPen);
			}
		}
	}

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Render(_hdc, _hWnd);
	}

	m_Player->Render(_hdc, _hWnd);

	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneStage04::Update(HWND _hWnd)
{
	if (m_Player->IsLife() == false)
	{
		m_Timer++; // 제한시간 안에 다시 원래대로 안돌리면 계속 증가함.
	}

	DefeatCheck(_hWnd);
	WinCheck(_hWnd);
	UpdateObjectFeature(_hWnd);

	m_BackgroundImage->Update(_hWnd);

	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].Update(_hWnd);
	}

	m_Player->Update(_hWnd, Objects);

	if (Stage04_isExit)
	{
		if (Stage04_babaIsWin == true)
		{
			m_FadeOut = true;
			m_NextGameState = STAGE_SELECT;
		}
		else if (Stage04_babaIsDead == true)
		{
			Init(m_hInstance);
		}
	}

	FadeUpdate();
}

void SceneStage04::SpriteUpdate(HWND _hWnd)
{
	for (auto idx = 0; idx < Objects.size(); idx++)
	{
		Objects[idx].SpriteUpdate(_hWnd);
	}

	m_Player->SpriteUpdate(_hWnd);
}

void SceneStage04::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		break;
	case WM_MOUSEMOVE:
		break;
	}
}

void SceneStage04::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			m_FadeOut = true;
			m_NextGameState = SETTING;
		}
		break;
		case VK_RETURN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
		}
		break;
		case VK_UP:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_DOWN:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().y + BLOCK_SIZE > (WINDOW_SIZE_Y - 39))
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_LEFT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x - BLOCK_SIZE < 0)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_RIGHT:
		{
			for (auto& v : Objects)
			{
				if (v.GetPawn())
				{
					if (v.GetPosition().x + BLOCK_SIZE > WINDOW_SIZE_X)
					{
						for (auto& v2 : Objects)
						{
							if (v2.GetPawn())
							{
								v2.SetState(OBJECTSTATE::STOPPED);
								v2.SetDirection(OBJECTDIRECTION::NONE);
								v2.SetMoveTimer(0);
							}
						}

						return;
					}

					v.KeyboardMessage(hWnd, message, wParam, lParam);
				}
			}

			m_Player->KeyboardMessage(hWnd, message, wParam, lParam);
			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		}
		break;
	}
}

void SceneStage04::StopAll()
{
	FadeStopAll();

	if (*m_CurrentGameState != SETTING)
		m_Sound->StopSound();
}

void SceneStage04::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneStage04::GetStageState(bool * state, int size)
{
	for (int idx = 0; idx < size; idx++)
	{
		m_StageState[idx] = state[idx];
	}
}

void SceneStage04::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

const bool SceneStage04::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneStage04::GetGridMode(bool * value)
{
	m_CurrentGridMode = value;
}

void SceneStage04::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneStage04::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneStage04::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{
	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneStage04::FadeIn()
{
	m_alpha -= ALPHA_SPEED;
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneStage04::FadeOut()
{
	m_alpha += ALPHA_SPEED;
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}

void SceneStage04::FadeStopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;
}

void SceneStage04::UpdateObjectFeature(HWND hWnd)
{
	if (Objects.size() != 0)
	{
		for (auto& v : Objects)
		{
			///////////////////////////////////////////////////////////////////////////////// PUSH 기준
			GameObject* target = &Objects[8];

			POINT targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			GameObject* targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			GameObject* targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == ROCK)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_SKULL) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == SKULL)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
			}


			target = &Objects[8];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == ROCK)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_SKULL) // ROKC IS PUSH
				{
					for (auto& v2 : Objects)
					{
						if (v2.GetKind() == SKULL)
						{
							v2.SetProperty(PROPERTY::PUSH);
						}
					}
				}
			}


			if ((targetObj1 != nullptr && targetObj2 == nullptr) || (targetObj1 == nullptr && targetObj2 != nullptr) || (targetObj1 == nullptr && targetObj2 == nullptr))
			{
				for (auto& v2 : Objects)
				{
					if (v2.GetKind() == SKULL || v2.GetKind() == ROCK)
					{
						v2.SetProperty(PROPERTY::OVERRIDE);
					}
				}
			}


			///////////////////////////////////////////////////////////////////////////////// DEFEAT 기준
			target = &Objects[11];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_SKULL) // SKULL IS DEFEAT
				{
					m_DefeatCondition = SKULL_IS_DEFEAT;
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS DEFEAT
				{
					m_DefeatCondition = ROCK_IS_DEFEAT;
				}
			}


			target = &Objects[11];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if (targetObj1 != nullptr && targetObj2 != nullptr)
			{
				if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_SKULL) // SKULL IS DEFEAT
				{
					m_DefeatCondition = SKULL_IS_DEFEAT;
				}
				else if (targetObj1->GetKind() == TEXT_KIND::TEXT_IS && targetObj2->GetKind() == TEXT_KIND::TEXT_ROCK) // ROKC IS DEFEAT
				{
					m_DefeatCondition = ROCK_IS_DEFEAT;
				}
			}


			target = &Objects[11];

			targetxy = POINT{ target->GetPosition().x, target->GetPosition().y };

			targetObj1 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE }, Objects);
			targetObj2 = target->GetObjects(POINT{ targetxy.x, targetxy.y - BLOCK_SIZE * 2 }, Objects);
			GameObject* targetObj3 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE, targetxy.y }, Objects);
			GameObject* targetObj4 = target->GetObjects(POINT{ targetxy.x - BLOCK_SIZE * 2, targetxy.y }, Objects);

			if ((targetObj1 == nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else
				{
					if (targetObj3->GetKind() == TEXT_KIND::TEXT_IS && targetObj4->GetKind() == TEXT_KIND::TEXT_SKULL)
					{
						m_DefeatCondition = SKULL_IS_DEFEAT;
					}
					else
					{
						m_DefeatCondition = CAN_NOT_DEFEAT;
					}
				}
			}
			else if ((targetObj1 != nullptr && targetObj2 == nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
			}
			else if ((targetObj1 == nullptr && targetObj2 != nullptr))
			{
				if ((targetObj3 == nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 != nullptr && targetObj4 == nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
				else if ((targetObj3 == nullptr && targetObj4 != nullptr))
				{
					m_DefeatCondition = CAN_NOT_DEFEAT;
				}
			}

			BabaIsDead(hWnd);
		}
	}
}

void SceneStage04::WinCheck(HWND hWnd)
{
	switch (m_WinCondition)
	{
	case FLAG_IS_WIN:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == FLAG)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						BabaIsWin(hWnd);
						m_PuzzleCompleted = true;
					}
				}
			}
		}
	}
	break;
	}
}

void SceneStage04::DefeatCheck(HWND hWnd)
{
	switch (m_DefeatCondition)
	{
	case SKULL_IS_DEFEAT:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == SKULL)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						m_Player->SetLife(false);
						m_Player->SetCanMove(false);
					}
				}
			}
		}
	}
	break;
	case ROCK_IS_DEFEAT:
	{
		if (Objects.size() != 0)
		{
			for (auto& v : Objects)
			{
				if (v.GetKind() == ROCK)
				{
					if ((m_Player->GetPosition().x == v.GetPosition().x) && (m_Player->GetPosition().y == v.GetPosition().y)) // 승리
					{
						m_Player->SetLife(false);
						m_Player->SetCanMove(false);
					}
				}
			}
		}
	}
	break;
	}
}

void SceneStage04::BabaIsDead(HWND hWnd)
{
	if (m_Timer > 60)
	{
		if (Stage04_isFirst)
		{
			Stage04_babaIsDead = true;
			Stage04_babaIsWin = false;
			Stage04_isFirst = false;
			m_DefeatSound->PlaySound(false, 1.0f);
			DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
		}

		m_Timer = 0;
	}
}

void SceneStage04::BabaIsWin(HWND hWnd)
{
	m_Player->SetLife(true);
	m_Player->SetCanMove(false);

	if (Stage04_isFirst)
	{
		Stage04_babaIsDead = false;
		Stage04_babaIsWin = true;
		Stage04_isFirst = false;
		m_Sound->StopSound();
		m_CompleteSound->PlaySound(false, 1.0f);
		DialogBox(m_hInstance, MAKEINTRESOURCE(IDD_DIALOG), hWnd, (DLGPROC)(DialogProc));
	}
}

const bool SceneStage04::GetPuzzleCompleted() const
{
	return m_PuzzleCompleted;
}

BOOL CALLBACK SceneStage04::DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hDialogDC, hDialogMemDC, hDialogBitDC;
	static RECT DialogCRT; // 직사각형모양 범 위 선언
	PAINTSTRUCT DialogPS;
	static HBITMAP hDialogBitmap, hDialogOldBitmap;

	static HBITMAP hButtonBitmap;

	static HWND _hWnd;

	static ObjectImage* DialogBackground;
	static 	ObjectImage* DialogNotifyDefeatImage;
	static 	ObjectImage* DialogNotifyConguratulationImage;
	static 	ObjectImage* DialogSpaceImage;
	static 	ObjectImage* DialogEnterImage;

	switch (message)
	{
	case WM_INITDIALOG:
	{
		_hWnd = (HWND)GetWindowLongPtrA(hWnd, GWLP_HWNDPARENT);
		GetClientRect(hWnd, &DialogCRT);

		DialogBackground = new ObjectImage();
		DialogBackground->LoadSpriteImage(L"Data/Images/Title/TitleBackground.png", 792, 432, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2), DialogCRT.right, DialogCRT.bottom);

		DialogNotifyDefeatImage = new ObjectImage();
		DialogNotifyDefeatImage->LoadSpriteImage(L"Data/Images/Notify_Defeat.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogNotifyConguratulationImage = new ObjectImage();
		DialogNotifyConguratulationImage->LoadSpriteImage(L"Data/Images/Notify_Congratulation.png", 400, 100, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) - 50, 400, 100);

		DialogSpaceImage = new ObjectImage();
		DialogSpaceImage->LoadSpriteImage(L"Data/Images/Space.png", 103, 50, 1, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 25, 70, 50);

		DialogEnterImage = new ObjectImage();
		DialogEnterImage->LoadSpriteImage(L"Data/Images/Map/Enter.png", 48, 24, 3, (DialogCRT.right / 2), (DialogCRT.bottom / 2) + 75, 70, 50);

		SetTimer(hWnd, 3, 1, NULL);
		SetTimer(hWnd, 4, 150, NULL);
	}
	break;
	case WM_TIMER:
	{
		switch (wParam)
		{
		case 3:
		{
			if (GetAsyncKeyState(VK_RETURN) & 0x8000)
			{
				Stage04_isExit = true;

				delete DialogBackground;
				delete DialogNotifyDefeatImage;
				delete DialogNotifyConguratulationImage;
				delete DialogSpaceImage;
				delete DialogEnterImage;

				EndDialog(hWnd, 0);
			}
		}
		break;
		case 4:
		{
			if (DialogBackground)
				DialogBackground->Update(hWnd);

			if (DialogNotifyDefeatImage)
				DialogNotifyDefeatImage->Update(hWnd);

			if (DialogNotifyConguratulationImage)
				DialogNotifyConguratulationImage->Update(hWnd);

			if (DialogSpaceImage)
				DialogSpaceImage->Update(hWnd);

			if (DialogEnterImage)
				DialogEnterImage->Update(hWnd);
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hDialogDC = BeginPaint(hWnd, &DialogPS);

		hDialogMemDC = CreateCompatibleDC(hDialogDC);
		hDialogBitmap = CreateCompatibleBitmap(hDialogDC, DialogCRT.right, DialogCRT.bottom);
		hDialogOldBitmap = (HBITMAP)SelectObject(hDialogMemDC, hDialogBitmap);
		FillRect(hDialogMemDC, &DialogCRT, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		if (DialogBackground)
			DialogBackground->Render(hDialogMemDC, hWnd);

		// 패배일 시
		if (DialogNotifyDefeatImage && Stage04_babaIsDead)
			DialogNotifyDefeatImage->Render(hDialogMemDC, hWnd);

		// 성공일 시
		if (DialogNotifyConguratulationImage && Stage04_babaIsWin)
			DialogNotifyConguratulationImage->Render(hDialogMemDC, hWnd);

		if (DialogSpaceImage)
			DialogSpaceImage->Render(hDialogMemDC, hWnd);

		if (DialogEnterImage)
			DialogEnterImage->Render(hDialogMemDC, hWnd);

		BitBlt(hDialogDC, 0, 0, DialogCRT.right, DialogCRT.bottom, hDialogMemDC, 0, 0, SRCCOPY);

		DeleteObject(SelectObject(hDialogMemDC, hDialogOldBitmap));
		DeleteDC(hDialogMemDC);

		EndPaint(hWnd, &DialogPS);
	}
	break;
	}
	return 0;
}