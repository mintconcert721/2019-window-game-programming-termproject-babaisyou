#include "stdafx.h"
#include "Baba.h"
#include "GameObject.h"

Baba::Baba()
{
	m_Position.x = 0;
	m_Position.y = 0;
	m_Size.x = BLOCK_SIZE;
	m_Size.y = BLOCK_SIZE;
	m_Velocity = 10;

	for (int i = 0; i < 5; i++) // 바바일 때 바뀔 수 있는 이미지 초기화
	{
		m_UpImage[i] = new ObjectImage();
		m_DownImage[i] = new ObjectImage();
		m_LeftImage[i] = new ObjectImage();
		m_RightImage[i] = new ObjectImage();
	}

	m_UpImage[0]->LoadSpriteImage(L"Data/Images/Baba/Up/baba_up_1.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_UpImage[1]->LoadSpriteImage(L"Data/Images/Baba/Up/baba_up_2.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_UpImage[2]->LoadSpriteImage(L"Data/Images/Baba/Up/baba_up_3.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_UpImage[3]->LoadSpriteImage(L"Data/Images/Baba/Up/baba_up_4.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_UpImage[4]->LoadSpriteImage(L"Data/Images/Baba/Up/baba_up_5.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);

	m_DownImage[0]->LoadSpriteImage(L"Data/Images/Baba/Down/baba_down_1.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_DownImage[1]->LoadSpriteImage(L"Data/Images/Baba/Down/baba_down_2.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_DownImage[2]->LoadSpriteImage(L"Data/Images/Baba/Down/baba_down_3.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_DownImage[3]->LoadSpriteImage(L"Data/Images/Baba/Down/baba_down_4.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_DownImage[4]->LoadSpriteImage(L"Data/Images/Baba/Down/baba_down_5.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);

	m_LeftImage[0]->LoadSpriteImage(L"Data/Images/Baba/Left/baba_left_1.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_LeftImage[1]->LoadSpriteImage(L"Data/Images/Baba/Left/baba_left_2.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_LeftImage[2]->LoadSpriteImage(L"Data/Images/Baba/Left/baba_left_3.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_LeftImage[3]->LoadSpriteImage(L"Data/Images/Baba/Left/baba_left_4.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_LeftImage[4]->LoadSpriteImage(L"Data/Images/Baba/Left/baba_left_5.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);

	m_RightImage[0]->LoadSpriteImage(L"Data/Images/Baba/Right/baba_right_1.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_RightImage[1]->LoadSpriteImage(L"Data/Images/Baba/Right/baba_right_2.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_RightImage[2]->LoadSpriteImage(L"Data/Images/Baba/Right/baba_right_3.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_RightImage[3]->LoadSpriteImage(L"Data/Images/Baba/Right/baba_right_4.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_RightImage[4]->LoadSpriteImage(L"Data/Images/Baba/Right/baba_right_5.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);

	m_WallImage = new ObjectImage();
	m_RockImage = new ObjectImage();

	m_WallImage->LoadSpriteImage(L"Data/Images/wall_1.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);
	m_RockImage->LoadSpriteImage(L"Data/Images/Tutorial/rock.png", 24, 24, 3, m_Position.x, m_Position.y, m_Size.x, m_Size.y);

	m_Kind = OBJECT_KIND::BABA;
}

Baba::~Baba()
{
	for (int idx = 0; idx < 5; idx++)
	{
		delete m_UpImage[idx];
		delete m_DownImage[idx];
		delete m_LeftImage[idx];
		delete m_RightImage[idx];
	}

	delete m_WallImage;
	delete m_RockImage;

}

void Baba::Init()
{
}

void Baba::Update(HWND _hWnd, vector<GameObject>& container)
{
	if (m_MyState == OBJECTSTATE::STOPPED)
	{
	}
	else if (m_MyState == OBJECTSTATE::MOVING)
	{
		if (m_MoveTimer <= 0)
		{
			m_MoveTimer = 0;
			m_MyState = OBJECTSTATE::STOPPED;
		}
		else
		{
			switch (m_Direction)
			{
			case OBJECTDIRECTION::UP:
			{
				// m_Position.y = m_Position.y - m_Velocity;
				if (!m_CanMove)
					return;

				if (CheckObjects(container))
				{
					if (!m_VariationFinished)
					{
						return;
					}
					m_Position.y = m_Position.y - m_Velocity;
				}
			}
			break;
			case OBJECTDIRECTION::DOWN:
			{
				// m_Position.y = m_Position.y + m_Velocity;
				if (!m_CanMove)
					return;

				if (CheckObjects(container))
				{
					if (!m_VariationFinished)
					{
						return;
					}
					m_Position.y = m_Position.y + m_Velocity;
				}
			}
			break;
			case OBJECTDIRECTION::LEFT:
			{
				// m_Position.x = m_Position.x - m_Velocity;
				if (!m_CanMove)
					return;

				if (CheckObjects(container))
				{
					if (!m_VariationFinished)
					{
						return;
					}
					m_Position.x = m_Position.x - m_Velocity;
				}
			}
			break;
			case OBJECTDIRECTION::RIGHT:
			{
				// m_Position.x = m_Position.x + m_Velocity;
				if (!m_CanMove)
					return;

				if (CheckObjects(container))
				{
					if (!m_VariationFinished)
					{
						return;
					}
					m_Position.x = m_Position.x + m_Velocity;
				}
			}
			break;
			}
		}

		m_MoveTimer -= m_Velocity;
	}
	
	switch (m_Kind)
	{
	case OBJECT_KIND::BABA:
	{
		for (int idx = 0; idx < 5; idx++)
		{
			m_UpImage[idx]->SetPosition(m_Position.x, m_Position.y);
			m_DownImage[idx]->SetPosition(m_Position.x, m_Position.y);
			m_LeftImage[idx]->SetPosition(m_Position.x, m_Position.y);
			m_RightImage[idx]->SetPosition(m_Position.x, m_Position.y);
		}
	}
	break;
	case OBJECT_KIND::ROCK:
	{
		m_RockImage->SetPosition(m_Position.x, m_Position.y);
	}
	break;
	case OBJECT_KIND::WALL :
	{
		m_WallImage->SetPosition(m_Position.x, m_Position.y);
	}
	break;
	}
}

void Baba::SpriteUpdate(HWND _hWnd)
{
	switch (m_Kind)
	{
	case OBJECT_KIND::BABA:
	{
		switch (m_Direction)
		{
		case OBJECTDIRECTION::UP:
		{
			m_UpImage[m_ImageIndex]->Update(_hWnd);
		}
		break;
		case OBJECTDIRECTION::DOWN:
		{
			m_DownImage[m_ImageIndex]->Update(_hWnd);
		}
		break;
		case OBJECTDIRECTION::LEFT:
		{
			m_LeftImage[m_ImageIndex]->Update(_hWnd);
		}
		break;
		case OBJECTDIRECTION::RIGHT:
		{
			m_RightImage[m_ImageIndex]->Update(_hWnd);
		}
		break;
		}
	}
	break;
	case OBJECT_KIND::ROCK:
	{
		m_RockImage->Update(_hWnd);
	}
	break;
	case OBJECT_KIND::WALL:
	{
		m_WallImage->Update(_hWnd);
	}
	break;
	}
}

void Baba::Render(HDC _hdc, HWND _hWnd)
{
	switch (m_Kind)
	{
	case OBJECT_KIND::BABA:
	{
		switch (m_Direction)
		{
		case OBJECTDIRECTION::UP:
		{
			m_UpImage[m_ImageIndex]->Render(_hdc, _hWnd);
		}
		break;
		case OBJECTDIRECTION::DOWN:
		{
			m_DownImage[m_ImageIndex]->Render(_hdc, _hWnd);
		}
		break;
		case OBJECTDIRECTION::LEFT:
		{
			m_LeftImage[m_ImageIndex]->Render(_hdc, _hWnd);
		}
		break;
		case OBJECTDIRECTION::RIGHT:
		{
			m_RightImage[m_ImageIndex]->Render(_hdc, _hWnd);
		}
		break;
		}
	}
	break;
	case OBJECT_KIND::ROCK:
	{
		m_RockImage->Render(_hdc, _hWnd);
	}
	break;
	case OBJECT_KIND::WALL:
	{
		m_WallImage->Render(_hdc, _hWnd);
	}
	break;
	}
}

void Baba::SetPosition(int positionX, int positionY)
{
	m_Position.x = positionX;
	m_Position.y = positionY;
}

void Baba::SetGoalPosition(int goalX, int goalY)
{
	m_GoalPosition.x = goalX;
	m_GoalPosition.y = goalY;
}

const POINT Baba::GetPosition() const
{
	return m_Position;
}

void Baba::SetSize(int sizeX, int sizeY)
{
	m_Size.x = sizeX;
	m_Size.y = sizeY;
}

void Baba::SetSize(int size)
{
	m_Size.x = size;
	m_Size.y = size;
}

const POINT Baba::GetSize() const
{
	return m_Size;
}

void Baba::SetKind(int value)
{
	m_Kind = value;
}

const int Baba::GetKind() const
{
	return m_Kind;
}

void Baba::SetState(bool value)
{
	m_MyState = value;
}

const bool Baba::GetState() const
{
	return m_MyState;
}

void Baba::SetDirection(OBJECTDIRECTION direction)
{
	m_Direction = direction;
}

const OBJECTDIRECTION Baba::GetDirection() const
{
	return m_Direction;
}

void Baba::SetMoveTimer(int value)
{
	m_MoveTimer = value;
}

const int Baba::GetMoveTimer() const
{
	return m_MoveTimer;
}

void Baba::SetLife(bool value)
{
	m_IsLife = value;
}

const bool Baba::IsLife() const
{
	return m_IsLife;
}

void Baba::SetCanMove(bool value)
{
	m_CanMove = value;
}

const bool Baba::IsCanMove() const
{
	return m_CanMove;
}

bool Baba::IsPointInRect(POINT pt, POINT obj_pt)
{
	if((obj_pt.x == pt.x) && (obj_pt.y == pt.y))
	{
		return true;
	}

	return false;
}

GameObject* Baba::GetObjects(POINT coord, vector<GameObject>& container)
{
	for (auto& v : container)
	{
		if (IsPointInRect(coord, v.GetPosition()))
		{
			return &v;
		}
	}

	return nullptr;
}

void Baba::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_MyState == OBJECTSTATE::STOPPED)
	{
		switch (message)
		{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_UP:
			{
				if (m_Position.y - m_WalkingDistance < 0)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::UP;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_DOWN:
			{
				if (m_Position.y + m_WalkingDistance > (WINDOW_SIZE_Y - 39))
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::DOWN;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_LEFT:
			{
				if (m_Position.x - m_WalkingDistance < 0)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::LEFT;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			case VK_RIGHT:
			{
				if (m_Position.x + m_WalkingDistance > WINDOW_SIZE_X)
				{
					return;
				}

				m_Direction = OBJECTDIRECTION::RIGHT;
				m_MyState = OBJECTSTATE::MOVING;

				m_MoveTimer = m_WalkingDistance;
			}
			break;
			}

			m_ImageIndex++;
			if (m_ImageIndex > 4)
				m_ImageIndex = 0;
		}
		break;
		}
	}
}

bool Baba::CheckObjects(vector<GameObject>& container)
{
	m_VariationFinished = true;
	switch (m_Direction)
	{
	case OBJECTDIRECTION::UP:
	{
		POINT xy = POINT{ m_Position.x, m_Position.y - m_WalkingDistance };

		GameObject* target = GetObjects(xy, container);
		if (target != nullptr)
		{
			if (target->GetProperty() == PROPERTY::STOP)
			{
				return false;
			}
			else if (target->GetProperty() == PROPERTY::PUSH)
			{
				bool res = target->FindNearObjects(this, target, container, OBJECTSTATE::MOVING, OBJECTDIRECTION::UP);
				return res;
			}
			else
			{

			}
		}
		else
		{
			return true;
		}
	}
	break;
	case OBJECTDIRECTION::DOWN:
	{
		POINT xy = POINT{ m_Position.x, m_Position.y + m_WalkingDistance };

		GameObject* target = GetObjects(xy, container);
		if (target != nullptr)
		{
			if (target->GetProperty() == PROPERTY::STOP)
			{
				return false;
			}
			else if (target->GetProperty() == PROPERTY::PUSH)
			{
				bool res = target->FindNearObjects(this, target, container, OBJECTSTATE::MOVING, OBJECTDIRECTION::DOWN);
				return res;
			}
			else
			{

			}
		}
		else
		{
			return true;
		}
	}
	break;
	case OBJECTDIRECTION::LEFT:
	{
		POINT xy = POINT{ m_Position.x - m_WalkingDistance, m_Position.y };

		GameObject* target = GetObjects(xy, container);
		if (target != nullptr)
		{
			if (target->GetProperty() == PROPERTY::STOP)
			{
				return false;
			}
			else if (target->GetProperty() == PROPERTY::PUSH)
			{
				bool res = target->FindNearObjects(this, target, container, OBJECTSTATE::MOVING, OBJECTDIRECTION::LEFT);
				return res;
			}
			else
			{

			}
		}
		else
		{
			return true;
		}
	}
	break;
	case OBJECTDIRECTION::RIGHT:
	{
		POINT xy = POINT{ m_Position.x + m_WalkingDistance, m_Position.y };

		GameObject* target = GetObjects(xy, container);
		if (target != nullptr)
		{
			if (target->GetProperty() == PROPERTY::STOP)
			{
				return false;
			}
			else if (target->GetProperty() == PROPERTY::PUSH)
			{
				bool res = target->FindNearObjects(this, target, container, OBJECTSTATE::MOVING, OBJECTDIRECTION::RIGHT);
				return res;
			}
			else
			{

			}
		}
		else
		{
			return true;
		}
	}
	break;
	}
}