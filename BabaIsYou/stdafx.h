﻿// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 특정 포함 파일이 들어 있는
// 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// 여기서 프로그램에 필요한 추가 헤더를 참조합니다.
#include <crtdbg.h>
#include <iostream>
#include <vector>
#include <iterator>
#include <chrono>

using namespace std;

/*#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif*/

#define WINDOW_SIZE_X (1600 + 16)
#define WINDOW_SIZE_Y (900 + 39)

extern int ScreenHalfX;
extern int ScreenHalfY;

// 장면 전환
#define TITLE 0
#define STAGE_SELECT 1
#define TUTORIAL 2
#define STAGE1 3
#define STAGE2 4
#define STAGE3 5
#define STAGE4 6
#define SETTING -1

// 그리드
#define GRID_GARO 32
#define GRID_SERO 18
#define BLOCK_SIZE 50

// 오브젝트 정보
#define KIND_STAGE_TUTORIAL 0
#define KIND_STAGE_1 1
#define KIND_STAGE_2 2
#define KIND_STAGE_3 3
#define KIND_STAGE_4 4
#define KIND_STAGE_5 5

#define KIND_TARGET 100
#define KIND_LINE 101

// Fade 효과
#define ALPHA_SPEED 51

enum TEXT_KIND {
	TEXT_BABA, TEXT_IS, TEXT_YOU, TEXT_FLAG, TEXT_WIN, TEXT_ROCK, TEXT_WALL, TEXT_PUSH, TEXT_STOP, TEXT_WATER, TEXT_SINK, TEXT_SKULL, TEXT_DEFEAT
};

//#define KIND_TEXT_BABA 200
//#define KIND_TEXT_IS 201
//#define KIND_TEXT_YOU 202
//#define KIND_TEXT_FLAG 203
//#define KIND_TEXT_WIN 204
//#define KIND_TEXT_ROCK 205
//#define KIND_TEXT_WALL 206
//#define KIND_TEXT_PUSH 207
//#define KIND_TEXT_STOP 208

enum OBJECT_KIND {
	FLAG = 100, ROCK, WALL, BABA, FLOWER, WATER, SKULL
};

//#define KIND_FLAG 301
//#define KIND_ROCK 302
//#define KIND_WALL 303

enum OBJECTSTATE {
	STOPPED, MOVING
};

enum OBJECTDIRECTION {
	NONE, UP, DOWN, LEFT, RIGHT
};

enum WINCONDITION {
	CAN_NOT_WIN, FLAG_IS_WIN, WALL_IS_WIN, ROCK_IS_WIN 
};

enum DEFEATCONDITION {
	CAN_NOT_DEFEAT, ROCK_IS_DEFEAT, SKULL_IS_DEFEAT
};

enum PROPERTY {
	PUSH, STOP, OVERRIDE
};

#include "ObjectImage.h"
#include "GameObject.h"
#include "Sound.h"