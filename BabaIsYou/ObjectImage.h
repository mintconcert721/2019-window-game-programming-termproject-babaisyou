#pragma once
#include <atlimage.h> // CImage 사용

class ObjectImage
{
private:
	CImage* m_Image;

	int m_SpriteX; // 스프라이트 가로 갯수
	int m_SpriteY; // 스프라이트 세로 갯수
	int m_SpriteEntireCount; // 스프라이트 전체 갯수
	int m_SpriteCurrentIndex; // 스프라이트 현재 인덱스 (초기는 0번째)

	int m_SpriteWidth; // 이미지 전체 가로길이를 가로 갯수로 나눈 개별 길이
	int m_SpriteHeight; // 이미지 전체 세로길이를 세로 갯수로 나눈 개별 길이

	int xCoord;
	int yCoord;

	POINT m_Position;
	POINT m_Size;

public:
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void Update(HWND _hWnd, int x, int y);
	void LoadSpriteImage(LPCTSTR fileName, int spriteGaroPixel, int spriteSeroPixel, int spriteEntireCount, int positionX, int positionY, int sizeX, int sizeY);
	void Destroy();
	void AlphaBlendRender(HDC _hdc, HWND _hWnd, BYTE _alpha);

	void SetPosition(int positionX, int positionY);
	const POINT GetPosition() const;
	void SetSize(int sizeX, int sizeY);
	const POINT GetSize() const;

public:
	ObjectImage();
	~ObjectImage();
};

