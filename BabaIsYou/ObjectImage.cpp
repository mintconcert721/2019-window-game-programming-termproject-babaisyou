#include "stdafx.h"
#include "ObjectImage.h"

ObjectImage::ObjectImage()
{
	m_Image = new CImage();

	m_SpriteX = 0;
	m_SpriteY = 0;
	m_SpriteEntireCount = 0;
	m_SpriteCurrentIndex = 0;

	m_Position.x = 0;
	m_Position.y = 0;

	m_Size.x = 0;
	m_Size.y = 0;
}

ObjectImage::~ObjectImage()
{
	delete m_Image;
}

// HDC, HWND, 그릴위치(가운데기준), 그릴크기
void ObjectImage::Render(HDC _hdc, HWND _hWnd)
{
	xCoord = m_SpriteCurrentIndex % m_SpriteWidth;
	yCoord = m_SpriteCurrentIndex / m_SpriteWidth;

	m_Image->Draw(_hdc, m_Position.x - (m_Size.x / 2), m_Position.y - (m_Size.y / 2), m_Size.x, m_Size.y, xCoord * m_SpriteX, yCoord * m_SpriteY, m_SpriteX, m_SpriteY);
}

void ObjectImage::Update(HWND _hWnd)
{
	(++m_SpriteCurrentIndex) %= m_SpriteEntireCount;
}

void ObjectImage::Update(HWND _hWnd, int x, int y)
{
	m_Position.x = x;
	m_Position.y = y;

	(++m_SpriteCurrentIndex) %= m_SpriteEntireCount;
}

void ObjectImage::LoadSpriteImage(LPCTSTR fileName, int spriteGaroPixel, int spriteSeroPixel, int spriteEntireCount, int positionX, int positionY, int sizeX, int sizeY)
{
	HRESULT retVal = m_Image->Load(fileName);

	// 이미지 로드 실패
	if (FAILED(retVal))
	{
		MessageBox(NULL, reinterpret_cast<LPCWSTR>(fileName), L"이미지 로드 실패", MB_ICONERROR);
		exit(0);
	}
	else
	{
		m_SpriteX = spriteGaroPixel;
		m_SpriteY = spriteSeroPixel;

		m_SpriteEntireCount = spriteEntireCount;

		m_SpriteWidth = m_Image->GetWidth() / m_SpriteX;
		m_SpriteHeight = m_Image->GetHeight() / m_SpriteY;

		m_Position.x = positionX;
		m_Position.y = positionY;

		m_Size.x = sizeX;
		m_Size.y = sizeY;
	}
}

void ObjectImage::Destroy()
{
	m_Image->Destroy();
}

void ObjectImage::AlphaBlendRender(HDC _hdc, HWND _hWnd, BYTE _alpha)
{
	m_Image->AlphaBlend(_hdc, m_Position.x - (m_Size.x / 2), m_Position.y - (m_Size.y / 2), m_Size.x, m_Size.y, 0, 0, m_SpriteX, m_SpriteY, _alpha, AC_SRC_OVER);
}

void ObjectImage::SetPosition(int positionX, int positionY)
{
	m_Position.x = positionX;
	m_Position.y = positionY;
}

const POINT ObjectImage::GetPosition() const
{
	return m_Position;
}

void ObjectImage::SetSize(int sizeX, int sizeY)
{
	m_Size.x = sizeX;
	m_Size.y = sizeY;
}

const POINT ObjectImage::GetSize() const
{
	return m_Size;
}