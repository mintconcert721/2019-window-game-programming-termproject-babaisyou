#include "stdafx.h"
#include "SceneTitle.h"

SceneTitle::SceneTitle()
{
	m_BackgroundImage = new ObjectImage();
	m_BackgroundImage->LoadSpriteImage(L"Data/Images/Title/TitleBackground.png", 792, 432, 3, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);
	m_TitleNameImage = new ObjectImage();
	m_TitleNameImage->LoadSpriteImage(L"Data/Images/Title/TitleName.png", 900, 290, 3, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 4), 750, 240);

	m_StartTextImage = new ObjectImage();
	m_StartTextImage->LoadSpriteImage(L"Data/Images/Title/Start.png", 427, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 25, 430, 50);
	m_SettingsTextImage = new ObjectImage();
	m_SettingsTextImage->LoadSpriteImage(L"Data/Images/Title/Settings.png", 428, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 85, 430, 50);
	m_ExitTextImage = new ObjectImage();
	m_ExitTextImage->LoadSpriteImage(L"Data/Images/Title/Exit.png", 427, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 145, 430, 50);

	m_StartTextHoveredImage = new ObjectImage();
	m_StartTextHoveredImage->LoadSpriteImage(L"Data/Images/Title/StartHovered.png", 427, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 25, 430, 50);
	m_SettingsTextHoveredImage = new ObjectImage();
	m_SettingsTextHoveredImage->LoadSpriteImage(L"Data/Images/Title/SettingsHovered.png", 428, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 85, 430, 50);
	m_ExitTextHoveredImage = new ObjectImage();
	m_ExitTextHoveredImage->LoadSpriteImage(L"Data/Images/Title/ExitHovered.png", 427, 49, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 145, 430, 50);

	m_CursorImage = new ObjectImage();
	m_CursorImage->LoadSpriteImage(L"Data/Images/Cursor.png", 24, 24, 3, (WINDOW_SIZE_X / 2) - 250, (WINDOW_SIZE_Y / 2) + 25, 50, 50);

	m_Sound = new Sound();
	m_Sound->CreateSound("Data/Sounds/Title/menu.ogg");
	m_Sound->PlaySound(true, 1.0f);

	m_MoveSound = new Sound();
	m_MoveSound->CreateSound("Data/Sounds/move.wav");

	m_StartButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 25);
	m_StartButton.SetSize(430, 50);
	m_SettingButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 85);
	m_SettingButton.SetSize(430, 50);
	m_ExitButton.SetPosition((WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2) + 145);
	m_ExitButton.SetSize(430, 50);

	m_CanEnterScene = true;
	m_SelectNumber = 0;

	m_FadeImage = new ObjectImage();
	m_FadeImage->LoadSpriteImage(L"Data/Images/Fade.png", 1600, 900, 1, (WINDOW_SIZE_X / 2), (WINDOW_SIZE_Y / 2), WINDOW_SIZE_X, WINDOW_SIZE_Y);
}

SceneTitle::~SceneTitle()
{
	delete m_BackgroundImage;
	delete m_TitleNameImage;

	delete m_StartTextImage;
	delete m_SettingsTextImage;
	delete m_ExitTextImage;

	delete m_StartTextHoveredImage;
	delete m_SettingsTextHoveredImage;
	delete m_ExitTextHoveredImage;

	delete m_CursorImage;
	delete m_Sound;
	delete m_MoveSound;

	delete m_FadeImage;
}

void SceneTitle::Init()
{
	m_Sound->PlaySound(true, 0.3f);

	FadeInit();
}

void SceneTitle::Render(HDC _hdc, HWND _hWnd)
{
	m_BackgroundImage->Render(_hdc, _hWnd);
	m_TitleNameImage->Render(_hdc, _hWnd);

	if(m_StartButton.IsMouseCollision(mouse))
		m_StartTextHoveredImage->Render(_hdc, _hWnd);
	else
		m_StartTextImage->Render(_hdc, _hWnd);

	if (m_SettingButton.IsMouseCollision(mouse))
		m_SettingsTextHoveredImage->Render(_hdc, _hWnd);
	else
		m_SettingsTextImage->Render(_hdc, _hWnd);
	
	if (m_ExitButton.IsMouseCollision(mouse))
		m_ExitTextHoveredImage->Render(_hdc, _hWnd);
	else
		m_ExitTextImage->Render(_hdc, _hWnd);

	m_CursorImage->Render(_hdc, _hWnd);
	FadeRender(_hdc, _hWnd, m_alpha);
}

void SceneTitle::Update(HWND _hWnd)
{
	switch (m_SelectNumber)
	{
	case 0:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 250, (WINDOW_SIZE_Y / 2) + 25);
		break;
	case 1:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 250, (WINDOW_SIZE_Y / 2) + 85);
		break;
	case 2:
		m_CursorImage->SetPosition((WINDOW_SIZE_X / 2) - 250, (WINDOW_SIZE_Y / 2) + 145);
		break;
	}

	FadeUpdate();
}

void SceneTitle::SpriteUpdate(HWND _hWnd)
{
	m_BackgroundImage->Update(_hWnd);
	m_TitleNameImage->Update(_hWnd);
	
	m_StartTextImage->Update(_hWnd);
	m_SettingsTextImage->Update(_hWnd);
	m_ExitTextImage->Update(_hWnd);

	m_StartTextHoveredImage->Update(_hWnd);
	m_SettingsTextHoveredImage->Update(_hWnd);
	m_ExitTextHoveredImage->Update(_hWnd);

	m_CursorImage->Update(_hWnd);
}

void SceneTitle::KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_CHAR:
	{
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_UP:
		{
			if (m_SelectNumber > 0)
			{
				m_SelectNumber--;
			}
			else
			{
				m_SelectNumber = 2;
			}

			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_DOWN:
		{
			if (m_SelectNumber < 2)
			{
				m_SelectNumber++;
			}
			else
			{
				m_SelectNumber = 0;
			}

			m_MoveSound->PlaySound(false, 1.0f);
		}
		break;
		case VK_ESCAPE:
			break;
		case VK_RETURN:
		{
			switch (m_SelectNumber)
			{
			case 0:
				m_FadeOut = true;
				m_NextGameState = STAGE_SELECT;
				break;
			case 1:
				m_FadeOut = true;
				m_NextGameState = SETTING;
				break;
			case 2:
				PostQuitMessage(0);
				break;
			}
		}
		break;
		}
		break;
	}
	break;
	}
}

void SceneTitle::MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (m_FadeIn)
	{
		if (!m_OngoingFadeInEffectFinished)
			return;
	}

	if (m_FadeOut)
	{
		if (!m_OngoingFadeOutEffectFinished)
			return;
	}

	switch (message)
	{
	case WM_LBUTTONDOWN:
	{
		::GetCursorPos(&mouse);
		ScreenToClient(hWnd, &mouse);

		if (m_StartButton.IsMouseCollision(mouse))
		{
			m_FadeOut = true;
			m_NextGameState = STAGE_SELECT;
			return;
		}

		if (m_SettingButton.IsMouseCollision(mouse))
		{
			m_FadeOut = true;
			m_NextGameState = SETTING;
			return;
		}
		
		if (m_ExitButton.IsMouseCollision(mouse))
		{
			PostQuitMessage(0);
		}
	}
	break;
	case WM_MOUSEMOVE:
	{
		::GetCursorPos(&mouse);
		ScreenToClient(hWnd, &mouse);
	}
	break;
	}
}

void SceneTitle::StopAll()
{
	m_FadeIn = false;
	m_alpha = 255;
	m_OngoingFadeInEffectFinished = false;

	m_Sound->StopSound();
}

void SceneTitle::GetGameState(int * state)
{
	m_CurrentGameState = state;
}

void SceneTitle::SetCanEnter(bool value)
{
	m_CanEnterScene = value;
}

const bool SceneTitle::GetCanEnter() const
{
	return m_CanEnterScene;
}

void SceneTitle::FadeInit()
{
	m_FadeIn = true;
	m_OngoingFadeInEffectFinished = false;
	m_OngoingFadeOutEffectFinished = false;
	m_FadeOut = false;
}

void SceneTitle::FadeUpdate()
{
	if (m_FadeIn)
	{
		FadeIn();
		if (m_OngoingFadeInEffectFinished)
		{
			m_FadeIn = false;
		}
	}

	if (m_FadeOut)
	{
		FadeOut();
		if (m_OngoingFadeOutEffectFinished)
		{
			*m_CurrentGameState = m_NextGameState;
			m_FadeOut = false;
		}
	}
}

void SceneTitle::FadeRender(HDC _hdc, HWND _hWnd, int _alpha)
{

	m_FadeImage->AlphaBlendRender(_hdc, _hWnd, _alpha);
}

void SceneTitle::FadeIn()
{
	m_alpha -= (ALPHA_SPEED / 2);
	if (m_alpha < 0)
	{
		m_alpha = 0;
		m_OngoingFadeInEffectFinished = true;
	}
}

void SceneTitle::FadeOut()
{
	m_alpha += (ALPHA_SPEED / 2);
	if (m_alpha > 255)
	{
		m_alpha = 255;
		m_OngoingFadeOutEffectFinished = true;
	}
}