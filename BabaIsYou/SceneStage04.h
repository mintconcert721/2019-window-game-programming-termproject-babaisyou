#pragma once

static bool Stage04_isExit = false;
static bool Stage04_isFirst = true;
static bool Stage04_babaIsDead = false;
static bool Stage04_babaIsWin = false;

class Sound;
class ObjectImage;
class GameObject;
class Baba;

class SceneStage04
{
private:
	ObjectImage* m_BackgroundImage;

	Sound* m_Sound;
	Sound* m_DefeatSound;
	Sound* m_MoveSound;
	Sound* m_CompleteSound;

	GameObject GridArray[GRID_GARO][GRID_SERO];
	vector<GameObject> Objects;

	HBRUSH hBrush;
	HBRUSH hOldBrush;
	HPEN hPen;
	HPEN hOldPen;

	Baba* m_Player;

	int* m_CurrentGameState;
	bool* m_CurrentGridMode;

	bool m_CanEnterScene;
	bool m_StageState[7];

	ObjectImage* m_FadeImage;

	bool m_FadeOut = false;
	bool m_FadeIn = true;
	bool m_OngoingFadeOutEffectFinished = false;
	bool m_OngoingFadeInEffectFinished = false;
	int m_alpha = 255;

	int m_NextGameState;

	int m_WinCondition = WINCONDITION::FLAG_IS_WIN;
	int m_DefeatCondition = DEFEATCONDITION::SKULL_IS_DEFEAT;

	HINSTANCE m_hInstance;
	int m_Timer = 0;

	bool m_PuzzleCompleted = false;

public:
	SceneStage04();
	~SceneStage04();

public:
	void Init(HINSTANCE _hInstance);
	void Render(HDC _hdc, HWND _hWnd);
	void Update(HWND _hWnd);
	void SpriteUpdate(HWND _hWnd);
	void MouseMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void KeyboardMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void StopAll();
	void GetGameState(int* state);
	void GetStageState(bool* state, int size);
	void SetCanEnter(bool value);
	const bool GetCanEnter() const;
	void GetGridMode(bool* value);

	void FadeInit();
	void FadeUpdate();
	void FadeRender(HDC _hdc, HWND _hWnd, int _alpha);
	void FadeIn();
	void FadeOut();
	void FadeStopAll();

	void UpdateObjectFeature(HWND hWnd);
	void WinCheck(HWND hWnd);
	void DefeatCheck(HWND hWnd);
	void BabaIsDead(HWND hWnd);
	void BabaIsWin(HWND hWnd);

	const bool GetPuzzleCompleted() const;

	static BOOL CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
};

